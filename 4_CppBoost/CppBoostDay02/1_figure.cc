#include <math.h>
#include <iostream>
#include <string>
#include <memory>

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;

//面向对象的设计原则：开放闭合原则
//对扩展开放，对修改关闭
//
//抽象类，作为接口使用
class Figure
{
public:
    //纯虚函数
    virtual void display() = 0;
    virtual double area() = 0;

    virtual ~Figure() {}
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length = 0, double width = 0)
    : _length(length)
    , _width(width)
    {
        cout << "Rectangle(double = 0, double = 0)" << endl;
    }

    void display() override
    {
        cout << "Rectangle";
    }

    double area() override
    {
        return _length * _width;
    }

    ~Rectangle()
    {
        cout << "~Rectangle()" << endl;
    }
private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double radius = 0)
    : _radius(radius)
    {
        cout << "Circle(double = 0)" << endl;
    }

    void display() override
    {
        cout << "Circle";
    }

    double area() override
    {
        return 3.14 * _radius *_radius;;
    }

    ~Circle()
    {
        cout << "~Circle()" << endl;
    }
private:
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a = 0, double b = 0, double c = 0)
    : _a(a)
    , _b(b)
    , _c(c)
    {
        cout << "Triangle(double = 0, double = 0, double = 0)" << endl;
    }

    void display() override
    {
        cout << "Triangle";
    }

    double area() override
    {
        //海伦公式
        double tmp = (_a + _b + _c)/2;

        return sqrt(tmp * (tmp - _a) * (tmp - _b) * (tmp - _c));
    }

    ~Triangle()
    {
        cout << "~Triangle()" << endl;
    }
private:
    double _a;
    double _b;
    double _c;
};

void func(Figure *pfig)
{
    pfig->display();
    cout << "的面积 : " << pfig->area() << endl;
}

//缺点：
//1、违反了单一职责原则
//2、违反了开放闭合原则
//3、违反了依赖导致原则
//
//静态工厂（简单工厂）
class Factory
{
public:
    static Figure *create(const string &name)
    {
        if(name == "rectangle")
        {
            //通过配置文件的形式将其读出来
            //配置文件的类型：txt、conf、xml、yang
            //读取配合文件，获取需要的数据
            //需要获取长与宽的值
            return  new Rectangle(10, 20);
        }
        else if(name == "circle")
        {
            //通过配置文件的形式将其读出来
            //配置文件的类型：txt、conf、xml、yang
            //读取配合文件，获取需要的数据
            //需要获取长与宽的值
            return new Circle(10);
        }
        else if(name == "triangle")
        {
            //通过配置文件的形式将其读出来
            //配置文件的类型：txt、conf、xml、yang
            //读取配合文件，获取需要的数据
            //需要获取长与宽的值
            return new Triangle(3, 4, 5);
        }
        else if(name == "yyy")
        {

        }
        else if(name == "xxx")
        {

        }
        else
        {
            return nullptr;
        }
    }
#if 0
    static Figure *createRectangle()
    {
    
        //通过配置文件的形式将其读出来
        //配置文件的类型：txt、conf、xml、yang
        //读取配合文件，获取需要的数据
        //需要获取长与宽的值
        Figure *pfig = new Rectangle(10, 20);
        return pfig;
    }
    
    static Figure *createCircle()
    {
    
        //通过配置文件的形式将其读出来
        //配置文件的类型：txt、conf、xml、yang
        //读取配合文件，获取需要的数据
        //需要获取长与宽的值
        Figure *pfig = new Circle(10);
        return pfig;
    }
    
    static Figure *createTriangle()
    {
    
        //通过配置文件的形式将其读出来
        //配置文件的类型：txt、conf、xml、yang
        //读取配合文件，获取需要的数据
        //需要获取长与宽的值
        Figure *pfig =  new Triangle(3, 4, 5);
        return pfig;
    }
#endif
};
#if 0
Figure *createRectangle()
{

    //通过配置文件的形式将其读出来
    //配置文件的类型：txt、conf、xml、yang
    //读取配合文件，获取需要的数据
    //需要获取长与宽的值
    Figure *pfig = new Rectangle(10, 20);
    return pfig;
}

Figure *createCircle()
{

    //通过配置文件的形式将其读出来
    //配置文件的类型：txt、conf、xml、yang
    //读取配合文件，获取需要的数据
    //需要获取长与宽的值
    Figure *pfig = new Circle(10);
    return pfig;
}

Figure *createTriangle()
{

    //通过配置文件的形式将其读出来
    //配置文件的类型：txt、conf、xml、yang
    //读取配合文件，获取需要的数据
    //需要获取长与宽的值
    Figure *pfig =  new Triangle(3, 4, 5);
    return pfig;
}
#endif
int main(int argc, char **argv)
{
    unique_ptr<Figure> prec(Factory::create("rectangle"));
    unique_ptr<Figure> pcir(Factory::create("circle"));
    unique_ptr<Figure> ptri(Factory::create("triangle"));

    func(prec.get());
    func(pcir.get());
    func(ptri.get());


    return 0;
}

