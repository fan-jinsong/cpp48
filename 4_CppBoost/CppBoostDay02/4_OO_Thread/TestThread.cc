#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyThread
: public Thread
{
/* public: */
private:
    void run() override
    {
        while(1)
        {
            cout << "MyThread is running!!!" << endl; 
            sleep(1);
        }
    }
};

void test()
{
    MyThread myth;
    myth.start();
    myth.join();
}

void test2()
{
    unique_ptr<Thread>  pth(new MyThread());
    pth->start();
    pth->join();
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

