#include "TcpServer.h"
#include "TcpConnection.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;

class MyTask
{
public:
    MyTask(const string &msg, const TcpConnectionPtr &con)
    : _msg(msg)
    , _con(con)
    {

    }
    void process()
    {
        //处理msg
        _msg;//就是需要进行处理的msg
        //进行业务逻辑的处理
        //.....
        //....
        //...
        //将数据的发送交给EventLoop
        //数据在线程池中处理好之后，不能直接使用send函数
        //将其发送给Reactor（EventLoop），发送数据的应该
        //是EventLoop对应的线程
        //Q:什么时候让线程池将数据发送给EventLoop？
        //A：线程池将任务处理好之后，就可以进行发送了
        //Q:如何发送呢？
        //A:可以在线程池中使用连接TcpConnection将数据发送
        //给EventLoop
        //Q:涉及到线程池与EventLoop之间的通信问题？
        //A:进程或者线程之间的通信可以使用eventfd的方法
        _con->sendInLoop(_msg);
    }

private:
    string _msg;
    TcpConnectionPtr _con;
};

//连接建立
void onConnection(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has connected!!!" << endl;
}

void onMessage(const TcpConnectionPtr &con)
{
    string msg = con->receive();
    //处理msg
    cout << ">>recv from client msg = " << msg << endl;
    //需要将msg进行处理之后，再将其发送给客户端
    //就需要做业务逻辑的处理，也就是处理msg
    
    MyTask task(msg, con);
    
    pool.addTask(std::bind(&MyTask::process, task));

    //数据发回给客户端
    con->send(msg);
}

void onClose(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has closed!!!" << endl;
}

void test()
{
    TcpServer server("127.0.0.1", 8888);
    server.setAllCallback(std::move(onConnection)
                          , std::move(onMessage)
                          , std::move(onClose));
    server.start();

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

