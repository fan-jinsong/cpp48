#include "head.h"
int main(int argc, char *argv[])
{
    // ./server 192.168.118.128 1234 3
    ARGS_CHECK(argc,4);
    int workernum = atoi(argv[3]);
    // 分配内存管理每个子进程的信息
    workerdata_t * workerdataArr = (workerdata_t *)calloc(workernum,sizeof(workerdata_t));
    // 创建子进程
    makeWorker(workernum,workerdataArr);
    return 0;
}

