#include "threadPool.h"
void cleanmutex(void *arg){
    threadPool_t * threadPool = (threadPool_t *)arg;
    pthread_mutex_unlock(&threadPool->taskQueue.mutex);
}
//void *threadFunc(void *arg){
//    threadPool_t * threadPool = (threadPool_t *)arg;
//    while(1){
//        int netfd;
//        // 准备取出任务
//        pthread_mutex_lock(&threadPool->taskQueue.mutex);
//        pthread_cleanup_push(cleanmutex,threadPool);
//        while(threadPool->taskQueue.queueSize == 0){
//            // 队列为空，子线程陷入等待
//            pthread_cond_wait(&threadPool->taskQueue.cond, &threadPool->taskQueue.mutex);
//        }
//        printf("worker thread get task!\n");
//        netfd = threadPool->taskQueue.pFront->netfd; // get queue top
//        taskDeQueue(&threadPool->taskQueue);
//        //pthread_mutex_unlock(&threadPool->taskQueue.mutex);
//        pthread_cleanup_pop(1);
//        // do download
//        transFile(netfd);
//        close(netfd);
//    }
//}
void *threadFunc(void *arg){
    threadPool_t * threadPool = (threadPool_t *)arg;
    while(1){
        int netfd;
        // 准备取出任务
        pthread_mutex_lock(&threadPool->taskQueue.mutex);
        while(threadPool->exitFlag == 0 && threadPool->taskQueue.queueSize == 0){
            // 队列为空，子线程陷入等待
            pthread_cond_wait(&threadPool->taskQueue.cond, &threadPool->taskQueue.mutex);
        }
        if(threadPool->exitFlag != 0){
            printf("child thread is going to exit!\n");
            pthread_mutex_unlock(&threadPool->taskQueue.mutex);
            pthread_exit(NULL);
        }
        printf("worker thread get task!\n");
        netfd = threadPool->taskQueue.pFront->netfd; // get queue top
        taskDeQueue(&threadPool->taskQueue);
        pthread_mutex_unlock(&threadPool->taskQueue.mutex);
        // do download
        transFile(netfd);
        close(netfd);
    }
}
int makeWorker(threadPool_t *threadPool){
    for(int i = 0; i < threadPool->threadnum; ++i){
        pthread_create(&threadPool->tidArr[i],NULL,threadFunc,threadPool);
    }
    return 0;
}
