#include "head.h"
int makeWorker(int workernum, workerdata_t * workerdataArr){
    // 创建子进程
    pid_t pid; // 存储fork的返回值
    int fds[2];// socketpair函数 必须写在fork之前
    for(int i = 0; i < workernum; ++i){
        socketpair(AF_LOCAL,SOCK_STREAM,0,fds);
        pid = fork();
        if(pid == 0){
            close(fds[0]);
            eventLoop(fds[1]);
        }// 让子进程永远离不开if的指令块
        //保存该子进程的信息
        close(fds[1]);
        workerdataArr[i].pid = pid;
        workerdataArr[i].status = FREE;
        workerdataArr[i].pipefd = fds[0];
        printf("worker %d, pid = %d, pipefd = %d\n", i, pid, fds[0]);
    }
    return 0;
}
int eventLoop(int sockfd){
    while(1){
        int netfd;
        int exitFlag;
        recvfd(sockfd,&netfd,&exitFlag); // 接收任务
        if(exitFlag != 0){
            printf("I am going to exit!\n");
            exit(0);
        }
        printf("I am going to work!\n"); // 执行任务
        //sleep(10);
        transFile(netfd);
        close(netfd);
        printf("work finished!\n");
        pid_t pid = getpid();
        send(sockfd,&pid,sizeof(pid),0); // 通知master任务完成
    }
}
