#include <48func.h>
//用于在master进程中存储每个worker进程的状态
enum {
    FREE,
    BUSY
};
typedef struct workerdata_s {
    pid_t pid; // 子进程的pid
    int status; // 子进程是BUSY还是FREE
    int pipefd; // 父进程和子进程通信使用的fd
} workerdata_t;
typedef struct train_s {
    int length;
    char data[1000]; // 1000只是上限，真实长度由length字段决定
} train_t;
int makeWorker(int workernum, workerdata_t * workerdataArr);
int eventLoop(int sockfd);
int tcpInit(const char *ip, const char *port, int *psockfd);
int epollAdd(int epfd, int fd);
int epollDel(int epfd, int fd);
int sendfd(int sockfd, int fdtosend, int exitFlag);
int recvfd(int sockfd, int *pfdtorecv, int *pexitFlag);
int transFile(int netfd);
