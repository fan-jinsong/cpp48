#include <48func.h>
void handler(int signum){
    printf("signum = %d\n", signum);
    time_t now = time(NULL);
    printf("handler now = %s\n", ctime(&now));
}
int main(int argc, char *argv[])
{
    time_t now = time(NULL);
    printf("now = %s\n", ctime(&now));
    struct itimerval itimer;
    itimer.it_value.tv_sec = 3;
    itimer.it_value.tv_usec = 0;
    itimer.it_interval.tv_sec = 1;
    itimer.it_interval.tv_usec = 0;
    //signal(SIGALRM,handler);
    //setitimer(ITIMER_REAL,&itimer,NULL);
    signal(SIGPROF,handler);
    setitimer(ITIMER_PROF,&itimer,NULL);
    while(1);
    return 0;
}

