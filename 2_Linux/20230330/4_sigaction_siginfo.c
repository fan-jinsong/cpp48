#include <48func.h>
void handler(int signum, siginfo_t *info, void *p){
    printf("signum = %d\n", signum);
    printf("pid = %d, uid = %d\n", info->si_pid, info->si_uid);
}
int main(int argc, char *argv[])
{
    //signal(SIGINT,handler);
    struct sigaction act;
    memset(&act,0,sizeof(act));
    act.sa_flags = SA_RESTART|SA_SIGINFO;//使用3参数版本的回调函数
    act.sa_sigaction = handler;
    sigaction(SIGINT,&act,NULL);
    char buf[1024] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

