#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_server_udpchat 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);// 创建一个udp的socket
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    int ret = bind(sockfd,(struct sockaddr *)&serveraddr,sizeof(serveraddr));
    ERROR_CHECK(ret,-1,"bind");
    struct sockaddr_in clientaddr;
    socklen_t socklen = sizeof(clientaddr);//socklen必须初始化
    char buf[4096] = {0};
    recvfrom(sockfd,buf,sizeof(buf),0,
             (struct sockaddr *)&clientaddr,&socklen);
    printf("client ip = %s, port = %d\n",
           inet_ntoa(clientaddr.sin_addr),
           ntohs(clientaddr.sin_port));
    printf("buf = %s\n",buf);
    fd_set rdset;
    while(1){
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO,&rdset);
        FD_SET(sockfd,&rdset);
        select(sockfd+1,&rdset,NULL,NULL,NULL);
        if(FD_ISSET(sockfd,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
            if(sret == 0){
                break;
            }
            printf("buf = %s\n", buf);
        }
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                // 发一个长度为0的消息
                sendto(sockfd,buf,0,0,
                       (struct sockaddr *)&clientaddr,
                       socklen);
                break;
            }
            sendto(sockfd,buf,strlen(buf),0,
                   (struct sockaddr *)&clientaddr,
                   socklen);
        }
    }
    close(sockfd);
    return 0;
}

