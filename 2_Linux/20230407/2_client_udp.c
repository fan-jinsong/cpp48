#include <48func.h>
int main(int argc, char *argv[])
{
    // ./2_client_udp 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);// 创建一个udp的socket
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    // 客户端先发消息 sendto
    sendto(sockfd,"zaima",5,0,
           (struct sockaddr *)&serveraddr,sizeof(serveraddr));
    char buf[1024] = {0};
    sleep(5);
    recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
    printf("buf = %s\n", buf);
    close(sockfd);
    return 0;
}

