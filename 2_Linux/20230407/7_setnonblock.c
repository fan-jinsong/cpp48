#include <48func.h>
int setnonblock(int fd){
    int flag = fcntl(fd,F_GETFL);//获取已经打开的文件的状态
    flag = flag|O_NONBLOCK;//增加一个非阻塞的属性
    int ret = fcntl(fd,F_SETFL,flag);//设置文件的状态
    ERROR_CHECK(ret,-1,"fcntl");
    return 0;
}
int main(int argc, char *argv[])
{
    char buf[2] = {0};
    setnonblock(STDIN_FILENO);
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(STDIN_FILENO,buf,1);
        printf("sret = %ld, buf = %s\n", sret, buf);
        sleep(1);
    }
    return 0;
}

