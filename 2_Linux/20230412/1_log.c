#include <48func.h>
#include "test/head.h"
int main(int argc, char *argv[])
{
    // __LINE__ 可以定位行号
    // __FUNCTION__ 可以定位所在的函数
    // __FILE__ 可以定位所在的文件
    printf("Line = %d\n", __LINE__);
    printf("Function = %s\n", __FUNCTION__);
    printf("File = %s\n", __FILE__);
    return 0;
}

