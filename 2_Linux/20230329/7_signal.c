#include <48func.h>
void handler(int signum){
    printf("signum is %d\n", signum);
    signal(SIGINT,SIG_DFL);
}
int main()
{
    sleep(5);
    printf("sleep over!\n");
    signal(2,handler);
    signal(SIGQUIT,handler);
    while(1);
    return 0;
}

