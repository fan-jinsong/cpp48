#include <48func.h>
#define NUM 10000000
typedef struct shareRes_s {
    int num;
    pthread_mutex_t mutex;
} shareRes_t;
void *threadFunc(void *arg){
    shareRes_t * pshareRes = (shareRes_t *)arg;
    for(int i = 0; i < NUM; ++i){
        pthread_mutex_lock(&pshareRes->mutex);
        ++pshareRes->num;
        pthread_mutex_unlock(&pshareRes->mutex);
    }
    pthread_exit(NULL);
}
int main()
{
    shareRes_t shareRes;
    shareRes.num = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);//初始化一个互斥锁，使用默认属性
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&shareRes);
    for(int i = 0; i < NUM; ++i){
        pthread_mutex_lock(&shareRes.mutex);
        ++shareRes.num;
        pthread_mutex_unlock(&shareRes.mutex);
    }
    pthread_join(tid,NULL);
    printf("num = %d\n", shareRes.num);
    pthread_mutex_destroy(&shareRes.mutex);
    return 0;
}

