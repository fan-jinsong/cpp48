#include <48func.h>
int main(int argc, char *argv[])
{
    // ./14_dup file1
    ARGS_CHECK(argc,2);
    int oldfd = open(argv[1],O_RDWR);
    ERROR_CHECK(oldfd,-1,"open");
    int newfd = dup(oldfd);
    printf("oldfd = %d, newfd = %d\n", oldfd, newfd);
    write(oldfd,"hello",5);
    write(newfd,"world",5);
    close(newfd);
    close(oldfd);
    return 0;
}

