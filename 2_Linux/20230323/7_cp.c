#include <48func.h>
int main(int argc, char *argv[])
{
    // ./7_cp src dest
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY|O_CREAT, 0666);
    ERROR_CHECK(fdw,-1,"open fdw");
    char buf[4096] = {0}; // char不是字符的意思，希望用长度为1字节的数据
    while(1){
        memset(buf,0,sizeof(buf));//好习惯 每次read之前先memset
        ssize_t sret = read(fdr,buf,sizeof(buf));
        if(sret == 0){
            break;
        }
        write(fdw,buf,sret);
    }
    close(fdw);
    close(fdr);
    return 0;
}

