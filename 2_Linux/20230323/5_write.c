#include <48func.h>
int main(int argc, char *argv[])
{
    // ./5_write file2
    ARGS_CHECK(argc,2);
    int fd = open(argv[1], O_RDWR|O_CREAT, 0666);
    ERROR_CHECK(fd,-1,"open");
    //char * buf  = "100000000";
    //ssize_t sret = write(fd,buf,strlen(buf));// read的count写大一点sizeof write的count有多少数据写多少数据
    //ERROR_CHECK(sret,-1,"write");
    int val = 100000000;
    ssize_t sret = write(fd,&val,sizeof(val));
    printf("sret = %ld\n", sret);
    return 0;
}

