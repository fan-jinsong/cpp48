#include <48func.h>
int main(int argc, char *argv[])
{
    // ./9_mmap file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    int ret = ftruncate(fd,5);
    ERROR_CHECK(ret,-1,"ftruncate");
    // 建立映射区 mmap第一个参数如果是NULL 自动从堆空间分配内存
    char *p = (char *)mmap(NULL,5,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");
    for(int i = 0; i < 5; ++i){
        printf("%c",*(p+i));
    }
    printf("\n");
    p[4] = 'o';
    munmap(p,5);
    close(fd);
    return 0;
}

