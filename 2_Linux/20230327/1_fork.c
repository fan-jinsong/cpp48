#include <48func.h>
int main()
{
    printf("Hello\n");
    pid_t ret = fork();
    if(ret == 0){
        // 只有子进程能够执行
        printf("I am child!, pid = %d, ppid = %d\n", getpid(), getppid());
    }
    else{
        // 只有父进程能够执行
        printf("I am parent!, pid = %d, ppid = %d\n", getpid(), getppid());
        sleep(1);
    }
    return 0;
}

