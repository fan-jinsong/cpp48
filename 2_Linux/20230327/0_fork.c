#include <48func.h>
int main()
{
    printf("Hello\n");
    pid_t ret = fork();
    if(ret == 0){
        // 只有子进程能够执行
        printf("I am child!\n");
    }
    else{
        // 只有父进程能够执行
        printf("I am parent!\n");
    }
    printf("World!\n");
    return 0;
}

