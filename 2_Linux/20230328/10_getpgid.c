#include <48func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        printf("I am child, pid = %d, ppid = %d, pgid = %d\n",
               getpid(),getppid(),getpgid(0));
    }
    else{
        printf("I am parent, pid = %d, ppid = %d, pgid = %d\n",
               getpid(),getppid(),getpgid(0));
        printf("bash process pgid = %d\n",getpgid(getppid()));
        wait(NULL);
    }
    return 0;
}

