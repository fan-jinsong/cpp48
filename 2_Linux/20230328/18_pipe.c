#include <48func.h>
int main(int argc, char *argv[])
{
    int pipefd[2];
    //pipe(pipefd[2]); pipefd[2] 代表下标为2的元素
    pipe(pipefd);
    printf("pipefd[0] = %d, pipefd[1] = %d\n", pipefd[0], pipefd[1]);
    write(pipefd[1],"niganma",7);
    char buf[1024] = {0};
    read(pipefd[0],buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

