#include <48func.h>
void Damemon(){
    // 创建一个新会话
    if(fork() != 0){
        exit(0);//父进程提前终止
    }
    setsid();
    // 关闭文件描述符
    for(int i = 0; i < 3; ++i){
        close(i);
    }
    // 修改其他进程的属性
    chdir("/");
    umask(0);
}
int main(int argc, char *argv[])
{
    Damemon();
    for(int i = 0; i < 30; ++i){
        time_t now = time(NULL);
        syslog(LOG_INFO,"Damemond %s",ctime(&now));
        sleep(2);
    }
    return 0;
}

