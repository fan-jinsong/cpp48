#include <48func.h>
int main()
{
    int fd = open("file1",O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    if(fork() == 0){
        // 子进程
        write(fd,"hello",5);
    }
    else{
        // 父进程
        sleep(1);
        write(fd,"world",5);
    }
    return 0;
}

