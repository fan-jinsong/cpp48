#include <48func.h>
int main()
{
    if(fork() == 0){
        printf("I am child, pid = %d, ppid = %d\n", getpid(),getppid());
        //return 123;
        while(1);
    }
    else{
        int wstatus;
        printf("I am parent, pid = %d, ppid = %d\n", getpid(),getppid());
        wait(&wstatus);
        if(WIFEXITED(wstatus)){
            printf("Normal exit! return value = %d\n", WEXITSTATUS(wstatus));
        }
        else if(WIFSIGNALED(wstatus)){
            printf("Been signaled! terminal signal = %d\n", WTERMSIG(wstatus));
        }
    }
}

