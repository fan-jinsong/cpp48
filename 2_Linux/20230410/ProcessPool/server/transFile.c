#include "head.h"
//int transFile(int netfd){
//    train_t train = {5, "file1"};
//    send(netfd,&train,sizeof(int)+train.length,0);// 发文件的名字
//    int fd = open("file1", O_RDWR);
//    ssize_t sret = read(fd,train.data,sizeof(train.data)); 
//    train.length = sret;
//    send(netfd,&train,sizeof(int)+train.length,0);// 发文件的内容
//    return 0;
//}
int transFile(int netfd){
    train_t train = {5, "file1"};
    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的名字
    int fd = open("file1", O_RDWR);
    struct stat statbuf;
    fstat(fd,&statbuf);
    train.length = sizeof(statbuf.st_size);
    memcpy(train.data,&statbuf.st_size,train.length);
    send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的大小
    while(1){
        bzero(&train,sizeof(train));
        ssize_t sret = read(fd,train.data,sizeof(train.data)); 
        train.length = sret;
        //即使文件已经读完了，也发一个空车厢火车过去
        //目标是让客户端知道文件已经发完了
        send(netfd,&train,sizeof(int)+train.length,MSG_NOSIGNAL);// 发文件的内容
        if(sret == 0){
            break;
        }
    }
    return 0;
}
