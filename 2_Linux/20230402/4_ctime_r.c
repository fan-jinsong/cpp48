#include <48func.h>
void *threadFunc(void *arg){
    time_t now = time(NULL);
    char buf[1024] = {0};
    char *p = ctime_r(&now,buf);
    printf("Before child thread, time = %s\n", p);
    sleep(5);
    printf("After child thread, time = %s\n", p);
    pthread_exit(NULL);
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    sleep(1);
    time_t mainNow = time(NULL);
    char buf[1024] = {0};
    char *p = ctime_r(&mainNow,buf);
    printf("main thread, time = %s\n", p);
    pthread_join(tid,NULL);
    return 0;
}

