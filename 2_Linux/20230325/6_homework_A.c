#include <48func.h>
int main(int argc, char *argv[])
{
    // ./6_homework_A 1.pipe file1
    ARGS_CHECK(argc,3);
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open fdw");
    int fd= open(argv[2],O_RDWR);
    ERROR_CHECK(fd,-1,"open fd");
    // 发文件的名字
    int size = strlen(argv[2]);
    write(fdw,&size,sizeof(size));
    write(fdw,argv[2],strlen(argv[2]));
    char buf[4096] = {0};
    ssize_t sret = read(fd,buf,sizeof(buf));
    size = sret;
    write(fdw,&size,sizeof(size));
    write(fdw,buf,size);
    return 0;
}

