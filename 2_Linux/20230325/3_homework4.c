#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_homework4 file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    ftruncate(fd,497);
    char *p = (char *)mmap(NULL,497,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    for(int i = 0; i < 497; ++i){
        //printf("%c", p[i]);
        if(p[i] >= 'a' && p[i] <= 'z'){
            //printf("%c",p[i]-32);
            p[i] = p[i] - 32;
        }
        else if(p[i] < 'A' || p[i] > 'Z'){
            //printf("%c", ' ');
            p[i] = ' ';
        }
        else{
            //printf("%c", p[i]);
        }
    }
    printf("\n");
    munmap(p,497);
    close(fd);
    return 0;
}

