#include <48func.h>
int main(int argc, char *argv[])
{
    // ./8_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    sleep(10);
    printf("sleep over!\n");
    char buf[8192];
    read(fdr,buf,sizeof(buf));
    sleep(120);
    close(fdr);
    return 0;
}

