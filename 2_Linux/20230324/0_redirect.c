#include <48func.h>
int main(int argc, char *argv[])
{
    // ./0_redirect file1
    ARGS_CHECK(argc,2);
    // 先打开文件
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    // 实现重定向
    printf("\n");
    close(STDOUT_FILENO);
    int newfd = dup(fd);
    printf("newfd = %d, fd = %d\n", newfd, fd);
    close(fd);
    return 0;
}

