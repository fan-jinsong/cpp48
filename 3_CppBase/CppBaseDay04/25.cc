 ///
 /// @file    25.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 12:01:39
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Student{
public:

	Student() {	cout << "Student()" << endl;	}
    Student(int number, const char * name ,int age)
    :_number(number)
    ,_name(new char[strlen(name) + 1]())
    ,_age(age)
    {
		strcpy(_name, name);
    }
    void print(){
        cout<<"number:"<<_number<<endl
            <<"name:"<<_name<<endl
            <<"age:"<<_age<<endl;
    }

	~Student() {
		if(_name) {
			delete [] _name;
			_name = nullptr;
		}
	}
private:
    int  _number;
    char * _name;
    int  _age;

};
int main (){
	char * str = "guanyu";//位于文字常量区,只读
	*str = 'G';//会导致段错误

    Student student(10,"liubei",60);
    student.print();

	Student * p = new Student;

	return 0;
}
