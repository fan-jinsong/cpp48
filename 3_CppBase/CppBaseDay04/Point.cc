 ///
 /// @file    Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 10:56:22
 ///
 
#include <iostream>
using std::cout;
using std::endl;


//单例模式的应用场景：
//1. C语言中的全局变量都可以用单例对象来替换
//2. 配置文件的信息，都可以存储在一个单例对象中
//3. 词典库、网页库都可以用单例对象来存储
//
//
//实现单例模式的步骤：
//1. 将构造函数私有化
//2. 定义了一个静态的成员函数
//3. 定义了一个静态的数据成员

class Point
{
private:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int,int)" << endl;	}

public:
	//返回值是对象，当执行return语句时，会调用
	//拷贝构造函数，对象是多个，因此不行
	static Point & getInstance()
	{
		//Point pt;//error
		if(_pInstance == nullptr) {
			_pInstance = new Point();
		}
		return  *_pInstance;
	}

	static void destroy()
	{
		if(_pInstance) {
			delete _pInstance;
			_pInstance = nullptr;
		}
	}

	void print() const 
	{
		cout << "(" << _ix 
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;
	int _iy;

	static Point * _pInstance;
};

Point * Point::_pInstance = nullptr;
 
void test0() 
{
	//可以生成无数个对象的，
	//因此该语句不能通过编译
	//Point pt1(1, 2);//构造函数私有化, 在类之外无法调用构造函数

	//Point pt2(3, 4);
	
	Point & ref = Point::getInstance();
	ref.print();
	cout << "&ref:" << &ref << endl;
 
	Point & ref2 = Point::getInstance();
	ref2.print();
	cout << "&ref2:" << &ref2 << endl;

	//delete &ref;//在形式上不够优雅
	Point::destroy();

	//在这里应该是再创建堆空间的对象
	Point::getInstance().print();

} 
 
int main(void)
{
	test0();
	return 0;
}
