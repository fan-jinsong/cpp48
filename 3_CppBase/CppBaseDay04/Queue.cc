 ///
 /// @file    Queue.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-22 10:18:47
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Queue
{
public:
	Queue(int size = 10)
	: _front(0)
	, _rear(0)
	, _size(size)
	, _data(new int[_size]())
	{}

	~Queue() {
		if(_data) {
			delete [] _data;
			_data = nullptr;
		}
	}

	bool empty() const
	{	return _front == _rear;	}

	bool full() const
	{	return (_rear + 1 ) % _size  == _front; }

	void push(int num) {
		if(!full()) {
			_data[_rear++] = num;
			_rear %= _size;
		} else {
			cout << "queue is full, cannot push any more data!" << endl;
		}
	}

	void pop() {
		if(!empty()) {
			++_front;
			_front %= _size;
		}
	}

	int front() const 
	{	return _data[_front];	}

	int back() const
	{	return _data[(_rear - 1 + _size) % _size];	}

private:
	int _front;
	int _rear;
	int _size;
	int * _data;
};
 
void test0() 
{
	Queue que;
	cout << "队列是否为空?" << que.empty() << endl;
	que.push(1);
	cout << "队列是否为空?" << que.empty() << endl;

	for(int i = 2; i < 12; ++i) {
		que.push(i);
	}
	cout << "队列是否已满?" << que.full() << endl;

	cout << "队头的元素是：" << que.front() << endl;
	cout << "队尾的元素是: " << que.back() << endl;

	que.pop();
	que.push(10);//下标为9的位置

	cout << "队头的元素是：" << que.front() << endl;
	cout << "队尾的元素是: " << que.back() << endl;

	while(!que.empty()) {
		cout << que.front() << endl;
		que.pop();
	}
	cout << "队列是否为空?" << que.empty() << endl;
 
} 
 
int main(void)
{
	test0();
	return 0;
}
