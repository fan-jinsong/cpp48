#include <iostream>
using std::cout;
using std::endl;
//智能指针的简单实现
template <class T>
class RAII
{
public:
    explicit
    RAII(T * data)
    : _data(data)
    {}

    RAII(const RAII &) = delete;
    RAII&operator=(const RAII&) = delete;

    T * operator->()
    {   return _data;   }

    T & operator*()
    {   return *_data;  }

    T * get() { return _data;   }


    ~RAII(){
        if(_data) {
            delete _data;
            _data = nullptr;
        }
    }

private:
    T * _data;//_data指向的就是堆空间的数据
}; 
class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    ~Point() {  cout << "~Point()" << endl; }

    void print() const
    {   cout << "(" << _ix << "," << _iy << ")" << endl;}

private:
    int _ix;
    int _iy;
};

void test0()
{
    RAII<Point> raii(new Point(1, 2));
    raii->print();
    //raii的对象可以直接转变成Point*
    (raii.operator->())->print();

    (*raii).print();

}


int main()
{
    test0();
    return 0;
}

