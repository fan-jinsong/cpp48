#include <memory> //vimplus快捷键  ,o
#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::unique_ptr;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {   cout << "Point(int,int)" << endl;   }

    ~Point() {  cout << "~Point()" << endl; }

    void print() const
    {   cout << "(" << _ix << "," << _iy << ")" << endl;}

private:
    int _ix;
    int _iy;
};  

unique_ptr<Point> getValue()
{
    //up是一个局部对象，即将被销毁
    unique_ptr<Point> up(new Point(3, 4));
    return up;//这里没有调用拷贝构造函数,调用的是移动构造函数
}

void test0()
{
    Point * pt = new Point(1, 2);
    unique_ptr<Point> up(pt);
    up->print();
    (*up).print();
    cout << "up.get(): " << up.get() << endl;
    cout << "pt: " << pt << endl;

    //不能进行复制或者赋值操作
    //unique_ptr<Point> up2(up);//error
    
    //getValue的返回值是一个右值,不是左值
    //会调用unqie_ptr的移动构造函数,创建对象up2
    unique_ptr<Point> up2 = getValue();
    up2->print();

    //unique_ptr可以放入容器中使用, 必须要进行转移操作
    vector<unique_ptr<Point>> pointers;
    pointers.push_back(std::move(up2));
}


int main()
{
    test0();
    return 0;
}










