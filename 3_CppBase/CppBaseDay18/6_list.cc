#include <iostream>
#include <vector>
#include <deque>
#include <list>

using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    list<int> number = {1, 3, 5, 7, 9, 8, 6, 4, 2};
    display(number);

    cout << endl << "在list的尾部进行插入与删除" << endl;
    number.push_back(13);
    number.push_back(20);
    display(number);
    number.pop_back();
    display(number);

    cout << endl << "在list的头部进行插入与删除" << endl;
    number.push_front(200);
    number.push_front(300);
    display(number);
    number.pop_front();
    display(number);

    cout << endl << "在list的任意位置进行插入" << endl;
    /* list<int>::iterator it = number.begin(); */
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 39);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, 100, 300);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    vector<int> vec = {111, 333, 666, 888, 222};
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it,  {12, 34, 56, 78});
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl << "list中清空元素" << endl;
    number.clear();
    cout << "number.size() = " << number.size() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

