#include <iostream>
#include <vector>
#include <deque>
#include <list>

using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 3, 5, 7, 9, 8, 6, 4, 2};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的尾部进行插入与删除" << endl;
    number.push_back(20);
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的任意位置进行插入" << endl;
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 39);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //insert扩容的时候，元素的个数是不定的，所以不能完全按照2倍
    //进行扩容，而push_back每次插入的元素的个数是固定的，也就是1
    //所以只要上一次的空间大于1，按照2倍扩容肯定没有问题
    //size() = m, capacity() = n, 待插入的元素的个数t
    //m = 11, n = 18
    //1、t < n - m,就不会扩容
    //2、n - m < t < m, 此时会按照2 * m扩容(7 < t < 11),
    //3、n - m < t, m < t < n , 此时会按照t + m扩容(11 < t < 18),
    //4、n - m < t, t > n , 此时会按照t + m扩容(t > 18),
    cout << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 30, 300);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    vector<int> vec = {111, 333, 666, 888, 222};
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

