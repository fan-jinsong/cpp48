#include <iostream>
#include <vector>
#include <deque>
#include <list>

using std::cout;
using std::endl;
using std::vector;
using std::deque;
using std::list;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    list<int> number = {1, 3, 5, 7, 9, 8};
    display(number);

    cout << endl << "list中的splice函数" << endl;
    list<int> number2 = {11, 77, 44, 55};
    auto it = number.begin();
    ++it;
    cout << "*it = " << *it <<endl;
    number.splice(it, number2);
    display(number);
    display(number2);
    cout << "*it = " << *it <<endl;

    cout << endl << "============" << endl;
    list<int> number3 = {100, 700, 400, 500, 600};
    auto it2 = number3.end();
    --it2;
    cout << "*it2 = " << *it2 << endl;
    number.splice(it, number3, it2);
    display(number);
    display(number3);
    cout << "*it = " << *it <<endl;

    cout << endl << "------------------------" << endl;
    auto it3 = number3.begin();
    auto it4 = number3.end();
    --it4;
    --it4;
    cout << "*it3 = " << *it3 << endl;
    cout << "*it4 = " << *it4 << endl;
    number.splice(it, number3, it3, it4);
    display(number);
    display(number3);

    cout <<endl << "测试splice在同一个链表中" << endl;
    auto it5 = number.end();
    --it5;
    cout << "*it5 = " << *it5 << endl;
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it <<endl;
    number.splice(it, number, it5);
    display(number);

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

