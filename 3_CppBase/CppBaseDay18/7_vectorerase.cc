#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 3, 4, 6, 6, 6, 6, 7, 8, 9};
    display(number);

    //想将vector中的所有元素等于6的删除
    for(auto it = number.begin(); it != number.end(); ++it)
    {
        if(6 == *it)
        {
            number.erase(it);
        }
    }
    display(number);
}

void test2()
{
    vector<int> number = {1, 3, 4, 6, 6, 6, 6, 7, 8, 9};
    display(number);

    //想将vector中的所有元素等于6的删除
    for(auto it = number.begin(); it != number.end(); )
    {
        if(6 == *it)
        {
            number.erase(it);
        }
        else
        {
            ++it;
        }
    }
    display(number);
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

