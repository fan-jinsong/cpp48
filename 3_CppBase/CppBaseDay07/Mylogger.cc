 ///
 /// @file    Mylogger.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-26 10:56:23
 ///
 

#include "Mylogger.hpp" //1. 自定义头文件第一位

//#include <stdio.h>   //2. C的头文件

#include <iostream>	// 3. C++ 头文件

//4. 第三方库头文件
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/Priority.hh>

using std::cout;
using std::endl;

namespace wd
{ 
Mylogger * Mylogger::_pInstance = nullptr;

using namespace log4cpp;
Mylogger::Mylogger()
: _mycat(Category::getRoot().getInstance("mycat"))
{
	auto ptn1 = new PatternLayout();
	ptn1->setConversionPattern("%d %c [%p] %m%n");
	auto ptn2 = new PatternLayout();
	ptn2->setConversionPattern("%d %c [%p] %m%n");

	auto pOsapp = new OstreamAppender("console", &cout);
	pOsapp->setLayout(ptn1);

	auto pFileapp = new FileAppender("pFileapp", "wd.log");
	pFileapp->setLayout(ptn2);

	_mycat.setPriority(Priority::DEBUG);
	_mycat.addAppender(pOsapp);
	_mycat.addAppender(pFileapp);
	cout << "Mylogger()" << endl;
}

Mylogger::~Mylogger()
{
	cout << "~Mylogger()" << endl;
	Category::shutdown();
}

void Mylogger::error(const char * msg)
{	_mycat.error(msg);	}

void Mylogger::warn(const char * msg)
{	_mycat.warn(msg);	}

void Mylogger::debug(const char * msg)
{	_mycat.debug(msg);	}

void Mylogger::info(const char * msg)
{	_mycat.info(msg);	}


}//end of namespace wd
