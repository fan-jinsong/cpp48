#include <iostream>

using std::cout;
using std::endl;

class NonCopyable
{
public:
    NonCopyable(){}
    NonCopyable(const NonCopyable &rhs)  = delete;
    NonCopyable &operator=(const NonCopyable &rhs)  = delete;

};

class Example
: public NonCopyable
{
public:
    Example(){}

    /* Example(const Example &rhs) = delete; */
private:
    /* Example(const Example &rhs); */

};

int main(int argc, char **argv)
{
    Example ex1;
    Example ex2 = ex1;//error

    return 0;
}

