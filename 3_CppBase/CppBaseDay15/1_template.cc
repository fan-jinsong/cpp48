#include <iostream>
using std::cout;
using std::endl;

//模板   
//class 在模板参数列表中表示类型参数
//<> 模板参数列表
//
//函数模板的实现原理: 当进行编译时， 根据实参的类型进行推导
//生成一个模板函数, 称为模板参数推导
//
//模板可以称为代码生成器
template <class Type>
Type add(Type x, Type y)
{
    return x + y;
}

#if 0
int add(int x, int y)
{   return x + y;   }

long add(long x, long y)
{   return x + y;   }

float add(float x, float y)
{   return x + y;   }
#endif

void test0()
{
    int a1 = 1, a2 = 2;
    long b1 = 10, b2 = 20;
    float c1 = 1.1, c2 = 2.2;
    cout << "add(a1, a2): " << add(a1, a2) << endl;
    cout << "add(b1, b2): " << add(b1, b2) << endl;
    cout << "add(c1, c2): " << add(c1, c2) << endl;
}


int main()
{
    test0();
    return 0;
}

