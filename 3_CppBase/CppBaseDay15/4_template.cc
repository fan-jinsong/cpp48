#include <iostream>
using std::cout;
using std::endl;

//函数模板
//模板参数只有1个
template <class Type>
Type add(Type x, Type y)
{
    cout << "Type add(Type, Type)" << endl;
    return x + y;
}

//模板参数有2个
template <class T, class S>
T add(T x, S y)
{
    return x + y;
}

//普通函数
int add(int x, int y)
{   
    cout << "int ad(int,int)" << endl;
    return x + y;   
}

#if 0
long add(long x, long y)
{   return x + y;   }

float add(float x, float y)
{   return x + y;   }
#endif

void test0()
{
    int a1 = 1, a2 = 2;
    long b1 = 10, b2 = 20;
    float c1 = 1.1, c2 = 2.2;
    cout << "add(a1, a2): " << add(a1, a2) << endl;
    cout << "add(b1, b2): " << add(b1, b2) << endl;
    cout << "add(c1, c2): " << add(c1, c2) << endl;
    cout << "add(a1, c2): " << add(a1, c2) << endl;
}


int main()
{
    test0();
    return 0;
}

