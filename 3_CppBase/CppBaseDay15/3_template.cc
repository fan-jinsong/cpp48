#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//函数模板
template <class Type>
Type add(Type x, Type y)
{
    cout << "Type add(Type, Type)" << endl;
    return x + y;
}

//普通函数
int add(int x, int y)
{   
    cout << "int ad(int,int)" << endl;
    return x + y;   
}

#if 0
long add(long x, long y)
{   return x + y;   }

float add(float x, float y)
{   return x + y;   }

#endif

//特化版本: 针对于某一个特殊类型，实现不同的版本
//1. 全特化:对于模板参数列表中的类型全部给出具体的类型
//2. 偏特化:对于模板参数列表中的部分类型给出具体的类型(不要求掌握)


//全特化的版本，不需要加上任何类型
//特化版本不能单独存在的,必须要跟着通用版本一起出现才能正常使用
template <>
const char * add<const char *>(const char * p1, const char * p2)
{   
    cout << "add<const char*>" << endl;
    char * ptmp = new char[strlen(p1) + strlen(p2) + 1]();
    strcpy(ptmp, p1);
    strcat(ptmp, p2);
    return ptmp;
}

void test0()
{
    int a1 = 1, a2 = 2;
    long b1 = 10, b2 = 20;
    float c1 = 1.1, c2 = 2.2;
    const char * pstr1 = "hellol";
    const char * pstr2 = "world";
    cout << "add(a1, a2): " << add(a1, a2) << endl;
    cout << "add(b1, b2): " << add(b1, b2) << endl;
    cout << "add(c1, c2): " << add(c1, c2) << endl;
    cout << "add(pstr1, pstr2):" << add(pstr1, pstr2) << endl;
}


int main()
{
    test0();
    return 0;
}

