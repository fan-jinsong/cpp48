#include <iostream>
using std::cout;
using std::endl;

template <class T>
void print(T t)
{
    cout << t << endl;
}


//print(args...)  => args在参数列表内部展开
//print(args)...  => print(arg1), print(arg2), print(argN)
//(print(args), 0)... 
//=> (print(arg1, 0), (print(args2,0), (print(argsN),0)

template <class... Args>
void printAll(Args...args)
{
    //利用逗号表达式的特性
    int arr[] = {
        (print(args), 0)...,
        /* (print(1), 0), */
        /* (print(1.2), 0), */
        /* (print('a'), 0), */
    };

    for(int i = 0; i < sizeof(arr)/sizeof(int); ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}

void test0()
{
    printAll(1, 2.2, 'a', "hello");
}


int main()
{
    test0();
    return 0;
}

