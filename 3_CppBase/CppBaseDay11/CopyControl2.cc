 ///
 /// @file    CopyControl.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-03 11:12:21
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class Base
{
public:
	Base()
	: _data(new char[1]())
	{	cout << "Base()" << endl;	}

	Base(const char * data)
	: _data(new char[strlen(data) + 1]())
	{
		strcpy(_data, data);
		cout << "Base(const char *)" << endl;
	}

	Base(const Base & rhs)
	: _data(new char[strlen(rhs._data) + 1]())
	{
		strcpy(_data, rhs._data);
		cout << "Base(const Base & )" << endl;
	}

	Base & operator=(const Base & rhs)
	{
		cout << "Base & operator=(const Base &)" << endl;
		if(this != &rhs) {
			delete [] _data;

			_data = new char[strlen(rhs._data) + 1]();
			strcpy(_data, rhs._data);
		}
		return *this;
	}


	~Base()
	{
		cout << "~Base()" << endl;
		if(_data) {
			delete [] _data;
			_data = nullptr;
		}
	}

	friend ostream & operator<<(ostream & os, const Base & rhs);
private:
	char * _data;
};

ostream & operator<<(ostream & os, const Base & rhs)
{
	os << rhs._data;
	return os;
}

class Derived
: public Base
{
public:
	Derived(const char * data, const char * data2)
	: Base(data)
	, _data2(new char[strlen(data2) + 1]())
	{	
		cout << "Derived(const char*, const char*)" << endl;	
		strcpy(_data2, data2);
	}

	Derived(const Derived & rhs)
	: Base(rhs) //显式调用基类拷贝构造函数
	, _data2(new char[strlen(rhs._data2) + 1]())
	{
		cout << "Derived(const Derived&)" << endl;
		strcpy(_data2, rhs._data2);
	}

	Derived & operator=(const Derived & rhs)
	{
		//const Base & rhs = rhs
		Base::operator=(rhs);//显式调用基类赋值运算符函数
		cout << "Derived & operator=(const Derived&)" << endl;
		if(this != &rhs) {
			delete [] _data2;
			_data2 = new char[strlen(rhs._data2) + 1]();
			strcpy(_data2, rhs._data2);
		}
		return *this;
	}

	~Derived() {
		cout << "~Derived()" << endl;
		if(_data2) {
			delete [] _data2;
			_data2 = nullptr;
		}
	}

	friend ostream & operator<<(ostream & os, const Derived & rhs);
private:
	char * _data2;
};
 
ostream & operator<<(ostream & os, const Derived & rhs)
{
	os << (Base &)rhs << ",";
	os << rhs._data2;
	return os;
}

void test0() 
{
	Derived d1("hello", "world");
	cout << "d1:" << d1 << endl;

	Derived d2 = d1;//拷贝构造函数
	cout << "d2:" << d2 << endl;

	Derived d3("wuhan", "wangdao");
	cout << "d3:" << d3 << endl;
 
	cout << "\n执行赋值操作:" << endl;
	d3 = d1;//赋值运算符函数
	cout << "d3:" << d3 << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}

















