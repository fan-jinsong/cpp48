#include <math.h>
#include <iostream>
#include <queue>
#include <vector>
#include <deque>

using std::cout;
using std::endl;
using std::priority_queue;
using std::vector;
using std::deque;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {

    }

    float getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
    friend bool operator<(const Point &lhs, const Point &rhs);
    friend bool operator>(const Point &lhs, const Point &rhs);
    friend struct ComparePoint;

private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
        << " ," << rhs._iy
        << ")";

    return os;
}

//重载运算符operator<
bool operator<(const Point &lhs, const Point &rhs)
{
    /* cout << "bool operator<(const Point &, const Point &)" << endl; */
    if(lhs.getDistance() < rhs.getDistance())
    {
        return true;
    }
    else if(lhs.getDistance() == rhs.getDistance())
    {
        if(lhs._ix < rhs._ix)
        {
            return true;
        }
        else if(lhs._ix == rhs._ix)
        {
            if(lhs._iy < rhs._iy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

//重载运算符operator>
bool operator>(const Point &lhs, const Point &rhs)
{
    /* cout << "bool operator>(const Point &, const Point &)" << endl; */
    if(lhs.getDistance() > rhs.getDistance())
    {
        return true;
    }
    else if(lhs.getDistance() == rhs.getDistance())
    {
        /* if(lhs.getX() > rhs.getX()) */
        if(lhs._ix > rhs._ix)
        {
            return true;
        }
        else if(lhs._ix == rhs._ix)
        {
            if(lhs._iy > rhs._iy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

//函数对象的形式
struct ComparePoint
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        /* cout << "struct ComparePoint " << endl; */
        if(lhs.getDistance() < rhs.getDistance())
        {
            return true;
        }
        else if(lhs.getDistance() == rhs.getDistance())
        {
            if(lhs._ix < rhs._ix)
            {
                return true;
            }
            else if(lhs._ix == rhs._ix)
            {
                if(lhs._iy < rhs._iy)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }
};

#if 1
//命名空间是可以进行扩展的
namespace std
{
//模板的特化(全特化)
template <>
struct less<Point>
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        /* cout << "struct std::less<Point> " << endl; */
        if(lhs.getDistance() < rhs.getDistance())
        {
            return true;
        }
        else if(lhs.getDistance() == rhs.getDistance())
        {
            if(lhs.getX() < rhs.getX())
            {
                return true;
            }
            else if(lhs.getX() == rhs.getX())
            {
                if(lhs.getY()< rhs.getY())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
};
}
#endif
void test()
{
    vector<Point> vec = {
        Point(1, 2),
        Point(1, -2),
        Point(-1, 2),
        Point(1, 2),
        Point(-1, 2),
        Point(5, 2),
        Point(3, 2),
    };

    /* priority_queue<Point> pque; */
    /* priority_queue<Point, vector<Point>, ComparePoint> pque; */
    /* priority_queue<Point, vector<Point>, std::greater<Point>> pque; */
    priority_queue<Point, std::deque<Point>, std::greater<Point>> pque;

    for(size_t idx = 0; idx != vec.size(); ++idx)
    {
        pque.push(vec[idx]);
        cout << "优先级最高的元素是 : " << pque.top() << endl;
    }

    while(!pque.empty())
    {
        cout << pque.top() << "  ";
        pque.pop();
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

