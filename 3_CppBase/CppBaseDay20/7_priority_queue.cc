#include <iostream>
#include <queue>
#include <vector>

using std::cout;
using std::endl;
using std::priority_queue;
using std::vector;

void test()
{
    //优先级队列使用的是堆排序，默认情况是一个大根堆（大顶堆）
    //当插入元素的时候，会将堆顶的元素与新插入的元素进行比较。如果
    //堆顶的元素比新插入的元素要小的话，正好满足std::less的特点；
    //那么新插入的元素会作为新的堆顶；如果堆顶比新插入的元素要大的
    //话，那么就不会满足std::less, 那么堆顶依旧是堆顶
    vector<int> vec = {1, 4, 7, 9, 6, 8, 4, 2};
    /* priority_queue<int> pque(vec.begin(), vec.end()); */
    priority_queue<int> pque;

    for(size_t idx = 0; idx != vec.size(); ++idx)
    {
        pque.push(vec[idx]);
        cout << "优先级最高的元素是 : " << pque.top() << endl;
    }

    while(!pque.empty())
    {
        cout << pque.top() << "  ";
        pque.pop();
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

