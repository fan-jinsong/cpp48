 ///
 /// @file    oversee.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-04 10:54:59
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
	Base(long base, long m)
	: _base(base)
	, _m(m)
	{	cout << "Base(long)" << endl;	}

	//这两个print方法发生了函数重载(overload)
	//函数重载发生在同一个类内部
	void print() const
	{	cout << "Base::_base:" << _base << endl;	}

	virtual
	void print(int x) const
	{	cout << "Base x: " << x << endl;}

protected:
	long _base;
	long _m;
};

class Derived
: public Base
{
public:
	Derived(long base, long derived, long m1, long m2)
	: Base(base, m1)
	, _derived(derived)
	, _m(m2)
	{	cout << "Derived(long,long)" << endl;	}

	//隐藏: 发生在父子类之间，父类中定义了一个函数,
	//子类中也定义一个同名函数，当通过子类对象调用
	//同名函数时，无法调用到基类的同名函数，不管有几个参数
	
	void print() const
	{	
		cout << "Derived:: _derived :" << _derived << endl;
		cout << "_m:" << _m << endl;//直接访问时，是派生类自己的
		cout << "Base::_m: " << Base::_m << endl;
	}

private:
	long _derived;
	long _m;
};
 
void test0() 
{
	/* Base base(10); */
	/* base.print(); */


	Derived derived(11, 100, 31, 32);
	derived.print();
	//derived.print(22);//error 派生类看不到基类的同名函数
	//derived.Base::print(22);//对于基类被隐藏的函数只能通过基类类名加上作用域限定符调用
} 
 
int main(void)
{
	test0();
	return 0;
}
