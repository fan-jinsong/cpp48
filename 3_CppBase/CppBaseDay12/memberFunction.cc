 ///
 /// @file    memberFunction.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-04 11:17:34
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
	Base(long base)
	: _base(base)
	{}

	virtual void print() const
	{	cout << "Base::_base:" << _base << endl;	}

	//在成员函数内部访问虚函数
	//表现动态多态(动态联编), 因为有一个this指针
	void func1()
	{	this->print();	}

	//如果只希望调用基类的虚函数
	void func2()
	{	Base::print();	}


private:
	long _base;
};

class Derived
: public Base
{
public:
	Derived(long base, long derived)
	: Base(base)
	, _derived(derived)
	{}

	void print() const override
	{
		cout << " Derived:: _derived:" << _derived << endl;
	}

private:
	long _derived;
};
 
void test0() 
{
	Base base(10);
	base.func1();

	Derived derived(11, 100);
	derived.func1();
	cout << endl;
	derived.func2();
 
} 
 
int main(void)
{
	test0();
	return 0;
}
