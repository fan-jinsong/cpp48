 ///
 /// @file    MultiDerived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 17:35:24
 ///
 
#include <iostream>
using std::cout;
using std::endl;


class A {
public:
	A() {cout <<"A()" << endl;}
	~A() {cout << "~A()" << endl;	}

	void print() const {	cout << "A::print()" << endl;	}
};

class B {
public:
	B() {cout <<"B()" << endl;}
	~B() {cout << "~B()" << endl;	}

	void display() const {	cout << "B::display()" << endl;	}
};

class C {
public:
	C() {cout <<"C()" << endl;}
	~C() {cout << "~C()" << endl;	}

	void show() const {	cout << "C::show()" << endl;	}
};

//默认继承方式是private的
class D
: public B
, public A
, public C
{
public:
	D()
	//对于基类被初始化的顺序与其在初始化表达式中的顺序无关，
	//只与其在被继承时的顺序有关
	: C()
	, B()
	, A()
	{	cout << "D()" << endl;	}
	
	~D() {	cout << "~D()" << endl;	}
};
 
void test0() 
{
	D d;
	d.print();
	d.display();
	d.show();
} 
 
int main(void)
{
	test0();
	return 0;
}
