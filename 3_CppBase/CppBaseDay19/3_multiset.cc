#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::multiset;
using std::vector;
using std::pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //multiset的特征
    //1、key值是不唯一的，可以重复
    //2、默认情况下，会按照key值升序排列
    //3、multiset的底层使用的是红黑树
    /* multiset<int, std::greater<int>> number = {1, 3, 7, 9, 8, 4, 3, 7}; */
    multiset<int> number = {1, 3, 7, 3, 3, 9, 8, 3, 4, 3, 7};
    display(number);

    cout << endl << "multiset中元素的查找操作" << endl;
    size_t cnt1 = number.count(3);
    size_t cnt2 = number.count(10);
    cout << "cnt1 = " << cnt1 << endl;
    cout << "cnt2 = " << cnt2 << endl;

    multiset<int>::iterator it = number.find(12);
    if(it == number.end())
    {
        cout << "该元素不存在multiset中" << endl;
    }
    else
    {
        cout << "该元素存在 = " << *it << endl;
    }

    cout << endl << "测试set的bound函数" << endl;
    auto it2 = number.lower_bound(3);
    auto it3 = number.upper_bound(3);
    cout << "*it2 = " << *it2 << endl;
    cout << "*it3 = " << *it3 << endl;
    while(it2 != it3)
    {
        cout << *it2 << "  ";
        ++it2;
    }
    cout << endl;
    pair<multiset<int>::iterator, multiset<int>::iterator>
        ret = number.equal_range(3);
    while(ret.first != ret.second)
    {
        cout << *ret.first << "  ";
        ++ret.first;
    }
    cout << endl;

    cout << endl << "multiset的insert操作" << endl;
    number.insert(5);
    display(number);

    cout << endl;
    vector<int> vec = {11, 33, 99, 2, 7, 19};
    number.insert(vec.begin(), vec.end());//左闭右开
    display(number);

    cout << endl;
    /* number.insert({6, 77, 22}); */
    number.insert(std::initializer_list<int>{6, 77, 22});
    display(number);

    cout << endl << "multiset的下标操作" << endl;
    /* cout << "number[1] = " << number[1] << endl; */

    cout << endl << "multiset的修改操作" << endl;
    it = number.begin();
    /* *it = 100;//error */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

