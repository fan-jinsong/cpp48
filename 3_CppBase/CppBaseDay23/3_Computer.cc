#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

class Computer
{
public:
    Computer(const char *brand, float price)
    : _brand(new char[strlen(brand) + 1]())
    , _price(price)
    {
        cout << "Computer(const char *, float)" << endl;
        strcpy(_brand, brand);
    }

    void print() const
    {
        cout << "_brand = " << _brand <<endl
             << "_price = " << _price << endl;
    }

    ~Computer()
    {
        cout << "~Computer()" << endl;
        if(_brand)
        {
            delete [] _brand;
            _brand = nullptr;
        }
    }

private:
    char *_brand;
    float _price;
};

void test()
{
    Computer com("xiaomi", 8888);
    cout <<"com = ";
    com.print();

    cout << endl << endl;
    com.~Computer();
    cout <<"com = ";
    com.print();

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

