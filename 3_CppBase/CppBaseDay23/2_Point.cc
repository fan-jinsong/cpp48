#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print() const
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;

};

void test()
{
    Point pt(1, 2);
    cout << "pt = ";
    pt.print();

    //不能以对象加点的形式显示调用构造函数
    /* pt.Point(1, 2);//error */

    //直接调用构造函数的
    Point(1, 2).print();//创建了一个对象，临时对象。匿名对象

    //析构函数是可以显示调用的，但是对于数据成员是内类
    //类型的时候，没有什么作用
    //析构虽然可以显示调用，但是一般不建议显示调用析构函数
    pt.~Point();
    /* ~Point();//error */
    cout << "pt = ";
    pt.print();
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

