#include <iostream>

using std::cout;
using std::endl;

/* #define CPP48TH */

int main(int argc, char **argv)
{
#ifndef CPP48TH  //if not define
    cout << "hello,CPP48TH" << endl;
#else
    cout << "hello,byby" << endl;
#endif
    return 0;
}

