 ///
 /// @file    9_constPoint.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 17:23:38
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{
		//_ix = ix;//error
		//_iy = iy;//error
	}

	void print()
	{
		cout << "(" << _ix
			 << "," << _iy 
			 << ")" << endl;
	}

private:
	//const成员必须要放在初始化表达式中进行初始化
	const int _ix;
	const int _iy;
};
 
void test0() 
{
	Point p(1, 2);
	p.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
