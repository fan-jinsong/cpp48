 ///
 /// @file    9_constPoint.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 17:23:38
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{
		cout << "Point(int=0,int=0)" << endl;
		//_ix = ix;//error
		//_iy = iy;//error
	}

	void print()
	{
		cout << "(" << _ix
			 << "," << _iy 
			 << ")";
	}

private:
	int _ix;
	int _iy;
};

class Line
{
public:
	Line(int x1, int y1, int x2, int y2)
	: _pt1(x1, y1)
	, _pt2(x2, y2)
	{
		cout << "Line(int,int,int,int)" << endl;
	}

	void printLine()
	{
		_pt1.print();
		cout << " ---> ";
		_pt2.print();
		cout << endl;
	}

private:
	//对象成员的初始化也要在初始化表达式中进行初始化
	Point _pt1;
	Point _pt2;
};
 
void test0() 
{
	Line line(1, 2, 3, 4);
	line.printLine();
} 
 
int main(void)
{
	test0();
	return 0;
}
