 ///
 /// @file    3_Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 10:21:04
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	//当类中定义了有参构造函数时，系统就不会再自动提供
	//默认构造函数了
	//
	//当需要继续调用默认构造函数时，则必须手动再提供一个
	//默认构造函数
	//
	//构造函数可以重载
	Point()
	{
		cout << "Point()" << endl;
		_ix = 0;
		_iy = 0;
	}

	Point(int ix, int iy)
	{
		cout << "Point()" << endl;
		_ix = ix;
		_iy = iy;
	}

	void print() 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

private:
	int _ix;
	int _iy;
};


 
void test0() 
{
	Point pt;
	pt.print();

	Point pt2(1, 2);
	cout << "pt2:";
	pt2.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
