 ///
 /// @file    3_Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-21 10:21:04
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{
		cout << "Point(int =0,int=0)" << endl;
	}

	Point(Point & rhs)
	: _ix(rhs._ix)
	, _iy(rhs._iy)
	{
		cout << "Point(const Point&)" << endl;
	}

	//系统会自动提供
	Point & operator=(const Point & rhs)
	{
		cout << "Point & operator=(const Point &)" << endl;
		_ix = rhs._ix;
		_iy = rhs._iy;
		return *this;//this代表的是左操作数
	}

	void print() 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

	~Point()
	{
		cout << "~Point()" << endl;
	}

private:
	int _ix;
	int _iy;
};

void test1()
{
	int a = 1, b = 2, c = 3;
	cout << &a << endl;

	cout << &(a = (b = c)) << endl;//赋值语句, a对b进行了复制操作

	//pt1和pt2对象都已经创建出来了
	Point pt1(1, 2);

	Point pt2(11, 12);
	//左操作数: pt1对象
	//右操作数: pt2对象
	pt1 = pt2;//调用赋值运算符函数
	//pt1.operator=(pt2);//完整形式

	cout << "pt1:";
	pt1.print();

	cout << "pt2:";
	pt2.print();
}

void test0() 
{
	int a = 1;
	int b = a;
	cout << "a:" << a << endl;
	cout << "b:" << b << endl;

	Point pt1(1, 2);
	//在这里是调用了拷贝构造函数
	//Point pt2 = pt1;
	Point pt2(pt1);
	cout << "pt1:";
	pt1.print();

	cout << "pt2:";
	pt2.print();

	Point pt3(11, 12);
	cout << "pt3:";
	pt3.print();

} 
 
int main(void)
{
	/* test0(); */
	test1();
	/* test2(); */
	/* test3(); */
	return 0;
}
