 ///
 /// @file    Computer.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 18:05:03
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{//大括号内部称为类内部
public:
	//在类内部只有函数声明
	void setBrand(const char * brand);
	void setPrice(float price);
	void print();

private:
	char _brand[20];
	float _price;
};
 

//在类之外实现成员函数
void Computer::setBrand(const char * brand)
{
	strcpy(_brand, brand);
}

void Computer::setPrice(float price)
{
	_price = price;
}

void Computer::print() 
{
	cout << "brand:" << _brand << endl;
	cout << "price:" << _price << endl;
}

void test0() 
{
	int number;//number称为变量,也可以称为对象
	Computer pc;//pc称为对象
	pc.setBrand("Hua wei matebook");
	pc.setPrice(8888);
	pc.print();

	cout << sizeof(Computer) << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
