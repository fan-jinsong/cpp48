 ///
 /// @file    Student.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 11:28:31
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Student
{
public:
	Student(int id, const char * name)
	: _id(id)
	, _name(new char[strlen(name) + 1]())
	{
		strcpy(_name, name);
		cout << "Student(int, const char*)" << endl;
	}


	~Student() {
		cout << "~Student()" << endl;
		if(_name) {

			delete [] _name;
			_name = nullptr;
		}
	}

	void print() const 
	{
		cout << "id:" << _id << endl
			 << "name:" << _name << endl;
	}



private:
	int _id;
	char * _name;
};
	//如果放在全局的位置，会修改所有类型的开辟空间的方式
	void * operator new(size_t sz)
	{
		cout << "void * operator new(size_t)" << endl;
		return malloc(sz);
	}

	void operator delete(void * p)
	{
		cout << "void operator delete(void*)" << endl;
		free(p);
	}
 
void test0() 
{
	Student * pstu = new Student(101, "Jackie");
	pstu->print();

	delete pstu;

	//对于栈对象来说，要同时要求构造函数和析构函数都在public区域
	/* Student s1(102, "Rose"); */
	/* s1.print(); */
} 
 
int main(void)
{
	test0();
	return 0;
}
