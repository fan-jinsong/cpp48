 ///
 /// @file    Student.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 11:28:31
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//要求：只能创建堆对象
//解决方案: 将析构函数放在私有区域即可

class Student
{
public:
//private:
	Student(int id, const char * name)
	: _id(id)
	, _name(new char[strlen(name) + 1]())
	{
		strcpy(_name, name);
		cout << "Student(int, const char*)" << endl;
	}


private:
	~Student() {
		cout << "~Student()" << endl;
		if(_name) {

			delete [] _name;
			_name = nullptr;
		}
	}

public:
	void print() const 
	{
		cout << "id:" << _id << endl
			 << "name:" << _name << endl;
	}

	void destroy() 
	{
		//this->~Student();
		delete this;
	}


//private:
	void * operator new(size_t sz)
	{
		cout << "void * operator new(size_t)" << endl;
		return malloc(sz);
	}

	void operator delete(void * p)
	{
		cout << "void operator delete(void*)" << endl;
		free(p);
	}


private:
	int _id;
	char * _name;
};

 
void test0() 
{
	Student * pstu = new Student(101, "Jackie");
	pstu->print();
	pstu->destroy();

	//delete pstu;//error 无法在类之外正常执行

	//对于栈对象来说，要同时要求构造函数和析构函数都在public区域
	//Student s1(102, "Rose");//error 该语句不能编译通过
	//s1.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
