 ///
 /// @file    Student.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 11:28:31
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//要求：该类只能生成栈对象
//解决方案: 将operator new库函数放在private区域即可

class Student
{
public:
//private:
	Student(int id, const char * name)
	: _id(id)
	, _name(new char[strlen(name) + 1]())
	{
		strcpy(_name, name);
		cout << "Student(int, const char*)" << endl;
	}


	~Student() {
		cout << "~Student()" << endl;
		if(_name) {

			delete [] _name;
			_name = nullptr;
		}
	}

public:
	void print() const 
	{
		cout << "id:" << _id << endl
			 << "name:" << _name << endl;
	}


private:
	void * operator new(size_t sz);
	void operator delete(void * p);


private:
	int _id;
	char * _name;
};

 
void test0() 
{
	//Student * pstu = new Student(101, "Jackie");//error
	//pstu->print();

	//delete pstu;

	//对于栈对象来说，要同时要求构造函数和析构函数都在public区域
	Student s1(102, "Rose");
	s1.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
