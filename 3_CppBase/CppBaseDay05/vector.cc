 ///
 /// @file    vector.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-24 14:40:40
 ///
 
#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
 
void test0() 
{
	//1. 无参构造函数产生的数组，
	//size和capacity 都是0
	//vector<int> numbers;
	//2. 第二种形式
	//vector<int> numbers(10, 1);

	//3. 第三种形式 迭代器 ==> 指针
	//迭代器指向的是容器中某一个元素的位置
	//int arr[5] = {1, 2, 3, 4, 5};
	//迭代器的范围是 左闭右开 [ , ) 的区间
	//vector<int> numbers(arr, arr + 5);
	//4. 大括号
	vector<int> numbers{1, 2, 3, 4, 5, 6};//C++11标准
	cout << "numbers' size:" << numbers.size() << endl;
	cout << "numbers's capacity:" << numbers.capacity() << endl;
	//遍历vector中的元素
	for(size_t i = 0; i < numbers.size(); ++i) {
		cout << numbers[i] << " ";
	}
	cout << endl;

	//引入迭代器的用法
	vector<int>::iterator it = numbers.begin();
	for(; it < numbers.end(); ++it) {
		cout << *it << " ";
	}
	cout << endl;

	for(auto & elem : numbers) {
		cout << elem << " ";
	}
	cout << endl;
} 

void display(const vector<int> & vec)
{
	cout << "vec'size:" << vec.size() << endl;
	cout << "vec'capacity:" << vec.capacity() << endl;
}

void test1()
{
	//动态扩容的步骤(GNU-GCC)：
	//1. 当发现size == capacity时，
	//先申请一个2 * capacacity的空间
	//2. 将原来空间中的所有元素拷贝到新空间
	//3. 释放原来的空间
	//4. 在新空间中新添加一个元素
	//
	//如果是VC++的编译器，它采用的方式是
	//动态扩容时，新申请的空间为1.5 *Capactiy
	vector<int> numbers;
	numbers.reserve(10);//只开空间，不添加元素
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);

	numbers.push_back(1);
	display(numbers);
	for(size_t i = 10; i < 100; ++i) {
		numbers.push_back(i);
	}

	display(numbers);
	cout << "sizeof(numbers):" << sizeof(numbers) << endl;
}
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}












