 ///
 /// @file    MultiDerived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 17:35:24
 ///
 
#include <iostream>
using std::cout;
using std::endl;


class A {
public:
	A() {cout <<"A()" << endl;}
	~A() {cout << "~A()" << endl;	}

	void print() const {	cout << "A::print()" << endl;	}
};

class B {
public:
	B() {cout <<"B()" << endl;}
	~B() {cout << "~B()" << endl;	}

	void print() const {	cout << "B::print()" << endl;	}
	void display() const {	cout << "B::display()" << endl;	}
};

class C {
public:
	C() {cout <<"C()" << endl;}
	~C() {cout << "~C()" << endl;	}

	void print() const {	cout << "C::print()" << endl;	}
	void show() const {	cout << "C::show()" << endl;	}
};

//默认继承方式是private的
class D
: public B
, public A
, public C
{
public:
	D()
	{	cout << "D()" << endl;	}
	
	~D() {	cout << "~D()" << endl;	}
};
 
void test0() 
{
	D d;
	//d.print();//成员名访问冲突的二义性
	d.A::print();
	d.B::print();
	d.C::print();
	d.display();//解决方案：使用作用域限定符
	d.show();
} 
 
int main(void)
{
	test0();
	return 0;
}
