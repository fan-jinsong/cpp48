 ///
 /// @file    Derived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 16:22:46
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
	Base(int ibase = 0)
	: _ibase(ibase)
	{	cout << "Base(int=0)" << endl;	}
	
	void print() const
	{	cout << "Base::_ibase = " << _ibase << endl;	}

protected:
	long _ibase;
};

class Derived
: public Base
{
public:
	//对于基类的非私有成员，虽然可以直接在派生类内部访问
	//当不能放在派生类的构造函数的初始化列表中进行访问
	Derived()
	: Base() //即使是无参构造函数，也显式给出来
	//: _ibase(0)//error, _ibase在派生类内部是protected型
	, _iderived(0)
	{	cout << "Derived()" << endl;	}

	Derived(int ibase1, int iderived)
	: Base(ibase1) //显式调用基类相应的构造函数
	, _iderived(iderived)
	{	cout << "Derived(int)" << endl;	}
	
	void display() const 
	{
		//_ibase在派生类内部是protected型,可以直接访问
		cout << "Base::_ibase:" << _ibase << endl;
		cout << "Derived::_iderived = " << _iderived << endl;
	}

private:
	long _iderived;
};
 
void test0() 
{
	Derived d1(10, 20);
	d1.print();
	d1.display();

	cout << "sizoef(Base):" << sizeof(Base) << endl;
	cout << "sizeof(Derived):" << sizeof(Derived) << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
