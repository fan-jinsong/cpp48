 ///
 /// @file    MultiDerived.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 17:35:24
 ///
 
#include <iostream>
using std::cout;
using std::endl;


class A {
public:
	A() {cout <<"A()" << endl;}
	~A() {cout << "~A()" << endl;	}

	void print() const {	cout << "A::print() _a:" << _a << endl;	}

	long _a;
};

class B 
: virtual public A{
public:
	B() {cout <<"B()" << endl;}
	~B() {cout << "~B()" << endl;	}

	void display() const {	cout << "B::display()" << endl;	}

	long _b;
};

class C 
: virtual public A {
public:
	C() {cout <<"C()" << endl;}
	~C() {cout << "~C()" << endl;	}

	void show() const {	cout << "C::show()" << endl;	}

	long _c;
};

class D
: public B
, public C
{
public:
	D()
	{	cout << "D()" << endl;	}
	
	~D() {	cout << "~D()" << endl;	}

	long _d;
};
 
void test0() 
{
	D d;
	d.print();//菱形继承的存储二义性问题
	//d.A::print();//解决方案：虚拟继承
	//d.B::print();
	//d.C::print();
	cout << "sizeof(d):" << sizeof(d) << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
