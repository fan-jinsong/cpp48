 ///
 /// @file    Point3D.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-02 15:46:01
 ///
 
#include <iostream>
using std::cout;
using std::endl;


//继承的局限：
//1. 构造函数、析构函数不能被继承
//2. 赋值运算符函数不能被继承
//3. operator new/delete 不能被继承
//4. 友元不能被继承

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int,int)" << endl;}

	void print() const 
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")" << endl;
	}

protected:
	//保护成员只能交给派生类直接访问
	int getx() const {	return _ix;	}

private:
	//私有成员只能在本类内部访问
	int _ix;
protected:
	int _iy;
};

class Point3D
: private Point //私有继承, 基类非私有成员在派生类内部都会变成private型
{
public:
	Point3D(int ix, int iy, int iz)
	: Point(ix, iy)
	, _iz(iz)
	{	cout << "Point3D(int,int,int)" << endl;	}

	void display() const 
	{
		//cout << "(" << _ix //不管以哪种方式继承，基类私有成员都无法在派生类中直接访问
		cout << "(" << getx()//private, 在派生类内部可以直接访问
			 << "," << _iy//private
			 << "," << _iz
			 << ")" << endl;
	}

private:
	int _iz;
};

class Point4D
: public Point3D
{
public:
	void show() const
	{	
		//getx无法访问
		cout << getx() << endl
			 << _iy << endl;
		
	}

private:
	int _im;
};
 
void test0() 
{	
	Point pt(10, 11);
	//pt.getx();//error

	Point3D pt3d(1, 2, 3);
	pt3d.display();
	//pt3d.print();//protected,派生类对象不能在保护继承时访问基类的public成员
	//pt3d.getx();//error, private型,不能直接在类之外访问
} 
 
int main(void)
{
	test0();
	return 0;
}
