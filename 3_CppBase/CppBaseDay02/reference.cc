 ///
 /// @file    reference.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 11:06:34
 ///
 
#include <iostream>
using std::cout;
using std::endl;
 
void test0() 
{
	int number = 1;
	//引用初始化的操作
	int & ref = number;//ref通过取地址符号得到的是绑定的实体的地址
	printf("&number: %p\n", &number);
	printf("&ref: %p\n", &ref);
	cout << "&number:" << &number << endl;

	ref = 10;//通过引用ref改变number的值, ref就是number
	cout << "number:" << number << endl;
	cout << "ref:" << ref << endl;

	int number2 = 100;
	ref = number2;//引用一经绑定之后，不会再改变指向，效果类似于指针常量
	cout << "&number2:" << &number2 << endl;
	printf("&ref: %p\n", &ref);

	//引用底层的实现还是指针, 只不过是编译器限制我们获取其地址
	//占据的空间还是一个指针的大小

	//int & ref2;//error 未进行初始化，引用不能单独存在
	//int * p;


	const int & ref3 = number2;
	//ref3 = 1000;//error, 不能修改引用的值
}
 
int main(void)
{
	test0();
	return 0;
}
