 ///
 /// @file    const.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 09:39:59
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//C语言中定义常量的方式
#define MAX 1024
 
void test0() 
{
	//C++更倾向于使用const关键字来定义常量
	const int num = 1;
	//num = 2;//error
	cout << "num : " << num << endl;

	cout << "MAX:" << MAX << endl;
} 

void test1() 
{
	int num1 = 1, num2 = 2;
	int * p1 = &num1;
	*p1 = 10;//1. 修改指针所指变量的值
	cout << "num1:" << num1 << endl;
	cout << "*p1:" << *p1 << endl << endl;

	p1 = &num2;//2. 改变指针的指向
	cout << "*p1:" << *p1 << endl;

	const int * p2 = &num1;//常量指针(pointer to const)
	//*p2 = 100;//error, 不能修改指针所指变量的值
	p2 = &num2;
	cout << "*p2:" << * p2 << endl;

	int const * p3 = &num1;
	//*p3 = 100;//error，效果与上面的是相同的
	p3 = &num2;

	int * const p4 = &num1;//指针常量(const pointer)
	*p4 = 100;//ok, 可以改变指针所指变量的值
	//p4 = &num2;//error， 不能改变指向

	//两者都不能进行修改
	const int * const p5 = &num1;
	//*p5 = 100;//error
	//p5 = &num2;//error
}
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
