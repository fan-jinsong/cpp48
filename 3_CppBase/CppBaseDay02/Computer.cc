 ///
 /// @file    Computer.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 18:05:03
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//类名的首字母要大写
class Computer
{//大括号内部称为类内部
//大括号以外的地方称为类外部
public:
	//公有的成员函数表示的是该类对外提供的接口、功能、服务
	void setBrand(const char * brand)
	{
		strcpy(_brand, brand);
	}

	void setPrice(float price)
	{
		_price = price;
	}

	void print() 
	{
		cout << "brand:" << _brand << endl;
		cout << "price:" << _price << endl;
	}

private:
	//1.数据成员的命名以下划线开头
	//m_brand
	//brand_
	//2. 对于私有成员建议放在类的底部
	char _brand[20];
	float _price;
};
 
void test0() 
{
	int number;
	Computer pc;
	pc.setBrand("Hua wei matebook");
	pc.setPrice(8888);
	pc.print();

	//私有成员不能直接在类之外访问
	//cout << pc._brand << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
