 ///
 /// @file    new.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 10:17:17
 ///
 
#include <iostream>
using std::cout;
using std::endl;

 
void test0() 
{
	int * p = (int*)malloc(sizeof(int));
	*p = 10;
	cout << "*p :" << *p << endl;

	free(p);//malloc/free成对出现
} 

void test1()
{
	int * p = new int(1);
	cout << "*p:" << *p << endl;

	//free(p);//error 不能使用free
	delete p;//释放的是单个变量

	//申请数组的空间
	int * p2 = new int[10](); //加上小括号的写法，是会进行初始化
	//int * p2 = new int[10];//对于空间中的数据值，是不确定的，有可能会出现脏数据
	for(int i = 0; i < 10; ++i) {
		cout << p2[i] << " ";
	}
	cout << endl;

	delete [] p2;//[]不能少，因为释放的是数组
}
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
