 ///
 /// @file    const_cast.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 14:50:42
 ///
 
#include <iostream>
using std::cout;
using std::endl;


void func(int * p)
{
	*p = 100;
	cout << "*p:" << *p << endl;
}
 
void test0() 
{
	const int number = 10;

	//无法用一个非const指针指向一个常量
	//int * p1 = &number;//error 
	//*p1 = 100;

	//const_cast是去除常量属性
	int * p1 = const_cast<int*>(&number);//ok
	//func内部的值是被修改了
	func(const_cast<int*>(&number));
	//但常量number的值没有被修改
	cout << "number:" << number << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
