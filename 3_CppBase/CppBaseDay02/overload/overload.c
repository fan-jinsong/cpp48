 ///
 /// @file    overload.c
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 15:02:56
 ///
 
#include <head.h>

int add(int x, int y)
{	return x + y;	}


long add2(long x, long y)
{	return x + y;	}
 
int main(int argc, char *argv[])
{
	int i1 = 1, i2 = 2;
	long l1 = 12, l2 = 13;

	printf("add(i1, i2): %d\n", add(i1, i2));
	printf("add(l1, l2): %d\n", add(l1, l2));
	return 0;
}
