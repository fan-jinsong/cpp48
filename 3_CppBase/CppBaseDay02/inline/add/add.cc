 ///
 /// @file    add.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-20 16:44:52
 ///
 
#include "add.hpp"

inline
int add(int x, int y)
{	return x + y;	}
