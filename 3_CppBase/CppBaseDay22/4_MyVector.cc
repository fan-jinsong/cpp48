#include <iostream>
#include <memory>
#include <algorithm>

using std::cout;
using std::endl;
using std::allocator;
using std::copy;

template<typename T>
class Vector
{
public:
    using iterator = T *;

    iterator begin()
    {
        return _start;
    }

    iterator end()
    {
        return _finish;
    }

    Vector()
    : _start(nullptr)
    , _finish(nullptr)
    , _end_of_storage(nullptr)
    {

    }

    ~Vector();

    void push_back(const T &); 
    void pop_back();    

    int size() const;
    int capacity() const;
private:
    void reallocate();//重新分配内存,动态扩容要用的
private:    
    static std::allocator<T> _alloc;//静态数据成员

    T * _start;      //指向数组中的第一个元素
    T * _finish; //指向最后一个实际元素之后的那个元素
    T * _end_of_storage;        //指向数组本身之后的位置
};

template<typename T>
std::allocator<T> Vector<T>::_alloc;//静态数据成员进行初始化

template<typename T>
Vector<T>::~Vector()
{
    if(_start)
    {
        while(_start != _finish)
        {
            //销毁对象
            _alloc.destroy(--_finish);
        }
        //空间回收
        _alloc.deallocate(_start, capacity());
    }
}

template<typename T>
void Vector<T>::push_back(const T &value)
{
    if(size() == capacity())
    {
        //需要进行扩容
        reallocate();
    }
    if(size() < capacity())
    {
        //构建对象，存放在vector尾部
        _alloc.construct(_finish++, value);
    }
}

template<typename T>
void Vector<T>::pop_back()
{
    if(size() > 0)
    {
        //将尾部的对象删除掉
        _alloc.destroy(--_finish);
    }
}

template<typename T>
int Vector<T>::size() const
{
    return _finish - _start;
}

template<typename T>
int Vector<T>::capacity() const
{
    return _end_of_storage - _start;
}

template<typename T>
void Vector<T>::reallocate()//重新分配内存,动态扩容要用的
{
    //两倍扩容的问题
    int oldCapacity = capacity();
    int newCacacity = oldCapacity > 0 ? 2 * oldCapacity: 1;

    //申请新的空间
    T *ptmp  = _alloc.allocate(newCacacity);//原始的未初始化的空间
    if(_start)
    {
        //将老的空间上的元素拷贝到新的空间上
        /* copy(_start, _finish, ptmp); */
        std::uninitialized_copy(_start, _finish, ptmp);

        //将老的空间进行回收
        //1、对象销毁
        while(_start != _finish)
        {
            /* _alloc.destroy(_start++); */
            _alloc.destroy(--_finish);
        }
        //2、空间回收
        _alloc.deallocate(_start, oldCapacity);
    }
    //将三个指针指向新的空间
    _start = ptmp;
    _finish = ptmp + oldCapacity;
    _end_of_storage = ptmp + newCacacity;
}

template <typename Container>
void printCapacity(const Container &con)
{
    cout << "con.size() = " << con.size() << endl;
    cout << "con.capacity() = " << con.capacity() << endl;
}

void test()
{
    Vector<int> number;
    printCapacity(number);

    cout << endl;
    number.push_back(1);
    printCapacity(number);

    cout << endl;
    number.push_back(2);
    printCapacity(number);

    cout << endl;
    number.push_back(3);
    printCapacity(number);

    cout << endl;
    number.push_back(4);
    printCapacity(number);

    cout << endl;
    number.push_back(5);
    printCapacity(number);

    cout << endl;
    number.push_back(6);
    printCapacity(number);

    cout << endl;
    number.push_back(7);
    printCapacity(number);

    cout << endl;
    number.push_back(5);
    printCapacity(number);

    cout << endl;
    number.push_back(5);
    printCapacity(number);

    cout << endl;
    for(auto &elem : number)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

