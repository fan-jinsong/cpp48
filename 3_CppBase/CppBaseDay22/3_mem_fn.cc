#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

using std::cout;
using std::endl;
using std::vector;
using std::for_each;
using std::copy;
using std::remove_if;

class Number
{
public:
    Number(int data = 0)
    : _data(data)
    {
    }

    void print() const
    {
        cout << _data << "  ";
    }

    //是不是偶数
    bool isEven() const
    {
#if 0
        int a = 10;
        if(10 == a )
        {

        }
        else
        {

        }
#endif
        return (0 == _data % 2);//魔数
    }

    //是不是质数
    bool isPrime() const
    {
        if(1 == _data)
        {
            return false;
        }
        //_data = 2, 3, 4, 8
        for(int idx = 2; idx <= _data/2; ++idx)
        {
            if(0 == _data%idx)
            {
                return false;
            }
        }

        return true;
    }

    ~Number()
    {
    }
private:
    int _data;
};

void test()
{
    vector<Number> vec;
    for(int idx = 1; idx != 30; ++idx)
    {
        vec.push_back(Number(idx));
    }

    Number num(10);
    using namespace std::placeholders;
    for_each(vec.begin(), vec.end(), std::bind(&Number::print, _1));
    /* for_each(vec.begin(), vec.end(), std::mem_fn(&Number::print)); */
    cout << endl;

    //所有的偶数删除
    cout << endl << "删除掉所有的偶数" << endl;
    auto it = remove_if(vec.begin(), vec.end(), 
                        std::mem_fn(&Number::isEven));
    vec.erase(it, vec.end());
    for_each(vec.begin(), vec.end(), std::mem_fn(&Number::print));
    cout << endl;

    cout << endl << "删除掉所有的质数" << endl;
    it = remove_if(vec.begin(), vec.end(), 
                        std::mem_fn(&Number::isPrime));
    vec.erase(it, vec.end());
    for_each(vec.begin(), vec.end(), std::mem_fn(&Number::print));
    cout << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

