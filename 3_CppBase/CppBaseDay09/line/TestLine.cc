 ///
 /// @file    TestLine.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 13:59:57
 ///
 
#include "Line.hpp"
#include <iostream>
using std::cout;
using std::endl;
 
void test0() 
{
	Line line(1, 2, 3, 4);
	line.printLine();

	//对于隐藏的嵌套类没法再直接创建对象了
	//Line::Point pt(10, 11);
	//cout << "pt:" << pt << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
