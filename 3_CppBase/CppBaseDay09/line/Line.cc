 ///
 /// @file    typeCast1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 10:19:24
 ///
 
#include "Line.hpp"

Line::Point::Point(int ix, int iy)
: _ix(ix)
, _iy(iy)
{
	cout << "Point(int,int)" << endl;
}

void Line::printLine() const
{
	cout << _pt1 << " --> " << _pt2 << endl;
}

std::ostream & operator<<(std::ostream & os, const Line::Point & rhs)
{
	os << "(" << rhs._ix
	   << "," << rhs._iy
	   << ")";
	return os;
}
