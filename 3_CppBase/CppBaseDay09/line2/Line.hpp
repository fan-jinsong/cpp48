 ///
 /// @file    Line.hpp
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 11:44:28
 ///
 
#ifndef __LINE_HPP__
#define __LINE_HPP__

//PIMPL设计模式
//好处：
//1. 实现了信息隐藏
//2. 将头文件 和动态库 交给第三方使用, 当实现文件有了变化（升级）时，
//	 只要头文件不变，第三方只需要替换动态库就可以了
//	 即可以完成库的平滑升级
//3. 编译防火墙
class Line
{
public:
	Line(int x1, int y1, int x2, int y2);
	void printLine() const;

	class LineImpl;//前向声明, 由LineImpl完成了信息隐藏
private:
	LineImpl * _pimpl;
};
 
#endif
