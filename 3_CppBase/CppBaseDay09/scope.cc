 ///
 /// @file    scope.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-28 11:26:14
 ///
 
#include <iostream>
using std::cout;
using std::endl;
 
 
int num = 1;
int x = 2;

class Example {
public:
    void print(int num)
    {
		//直接访问num的时候，将其他的同名num进行了屏蔽
		//遵循的是一个就近原则
		//建议别这样进行命名
        cout << "形参num = " << num << endl;
        cout << "数据成员num = " << this->num << endl;
        cout << "数据成员num = " << Example::num << endl;
        cout << "全局变量num = " << ::num << endl;
    }
private:
    int num;
};

void test0() 
{
	Example e;
	e.print(10);
} 
int main(void)
{
	test0();
	return 0;

}
