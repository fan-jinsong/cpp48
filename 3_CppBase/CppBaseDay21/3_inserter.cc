#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <set>
#include <algorithm>

using std::cout;
using std::endl;
using std::back_inserter;
using std::back_insert_iterator;
using std::front_inserter;
using std::front_insert_iterator;
using std::inserter;
using std::insert_iterator;
using std::vector;
using std::list;
using std::set;
using std::copy;
using std::ostream_iterator;

void test()
{
    vector<int> vecNumber = {1, 3, 5, 7};
    list<int> listNumber = {11, 33, 77, 99};
    //back_inserter以及back_insert_iterator底层会调用push_back
    /* copy(listNumber.begin(), listNumber.end(), back_inserter(vecNumber)); */
    copy(listNumber.begin(), listNumber.end(), 
         back_insert_iterator<vector<int>>(vecNumber));
    copy(vecNumber.begin(), vecNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl;
    //front_inserter以及front_insert_iterator底层会调用push_front
    /* copy(vecNumber.begin(), vecNumber.end(), front_inserter(listNumber)); */
    copy(vecNumber.begin(), vecNumber.end(), 
         front_insert_iterator<list<int>>(listNumber));
    copy(listNumber.begin(), listNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    //inserter以及insert_iterator底层会调用inserter
    set<int> setNumber = {1, 5, 9, 7, 11, 24, 56};
    set<int>::iterator it = setNumber.begin();
    /* copy(vecNumber.begin(), vecNumber.end(), */ 
    /*      inserter<set<int>>(setNumber, it)); */
    copy(vecNumber.begin(), vecNumber.end(), 
         insert_iterator<set<int>>(setNumber, it));
    copy(setNumber.begin(), setNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

