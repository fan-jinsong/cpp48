#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

int func1()
{
    return 10;
}

int func2()
{
    return 20;
}

int func3(int x)
{
    return x;
}

void test()
{
    typedef int B;

    //函数指针的特点：
    /* typedef int(*)()  pf; */
    //延迟函数调用的思想
    typedef int(*pf)();//pf就是一个类型
    pf f  = &func1;//注册回调函数
    //...
    //...
    cout << "f() = " << f() << endl;//执行回调函数

    pf f2 = func2;
    cout << "f2() = " << f2() << endl;

    /* pf f3 = func3;//error */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

