#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

void func(int x1, int x2, int x3, const int &x4, int x5)
{
    cout << "x1 = " << x1 << endl
         << "x2 = " << x2 << endl
         << "x3 = " << x3 << endl
         << "x4 = " << x4 << endl
         << "x5 = " << x5 << endl;
}

void test()
{
    int number = 10;
    using namespace std::placeholders;
    //占位符本身会表示的是形参的位置
    //占位符中的数字代表的是实参的位置
    //bind绑定函数的时候，默认会使用值传递
    //std::ref = referenece，称为引用的包装器
    //std::cref = const referenece，称为引用的包装器
    auto f = bind(func, 1, _3, _1, std::cref(number), number);
    number = 100;
    f(20, 40, 50, 60, 80);//多余的参数就直接丢掉
    /* f(20, 40, 50); */
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

