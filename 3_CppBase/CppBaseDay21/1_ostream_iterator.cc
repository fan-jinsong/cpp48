#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::copy;

//输出流运算符<<在哪里出现的?
//operator<<
void test()
{
    vector<int> vec = {1, 3, 5, 7, 9};
    ostream_iterator<int> osi(cout, "\n");
    copy(vec.begin(), vec.end(), osi);
    /* cout << 1 << "\n"; */
    /* cout << 3 << "\n"; */

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

