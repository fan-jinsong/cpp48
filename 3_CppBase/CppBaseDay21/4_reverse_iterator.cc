#include <iostream>
#include <iterator>
#include <vector>

using std::cout;
using std::endl;
using std::reverse_iterator;
using std::vector;

void test()
{
    vector<int> number = {1, 4, 7, 9, 45, 234};
    vector<int>::iterator it = number.begin();
    for(; it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
}

void test2()
{
    vector<int> number = {1, 4, 7, 9, 45, 234};
    vector<int>::reverse_iterator rit = number.rbegin();
    for(; rit != number.rend(); ++rit)
    {
        cout << *rit << "  ";
    }
    cout << endl;
}

int main(int argc, char **argv)
{
    test2();
    return 0;
}

