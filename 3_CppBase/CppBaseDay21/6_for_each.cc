#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::copy;
using std::count;
using std::vector;
using std::ostream_iterator;

void test()
{
    vector<int> number = {1, 2, 5, 7, 5, 3, 5, 3, 6, 7};
    size_t cnt = count(number.begin(), number.end(), 5);
    cout << "cnt = " << cnt << endl;

    cout <<endl;
    vector<int>::iterator it = find(number.begin(), number.end(), 3);
    if(it == number.end())
    {
        cout << "查找失败，该元素不在vector中"  << endl;
    }
    else
    {
        cout << "查找成功 " << *it << endl;
    }

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

