#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::copy;
using std::count;
using std::vector;
using std::ostream_iterator;

bool func(int value)
{
    return value > 5;
}

void test()
{
    vector<int> number = {1, 3, 5, 6, 5, 7, 8, 9, 2, 4};
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl << "执行remove_if函数" << endl;
    //remove_if并不能将满足条件的元素删除，但是会返回待删除的元素
    //的首迭代器，然后再配合着erase进行删除，就能满足要求
    //为什么这么设计？
    //因为算法库中的算法不是针对于某一种具体容器设置，为了让所有的
    //容器都可以使用remove_if而不出现问题.
    auto it = remove_if(number.begin(), number.end(), func);
    number.erase(it, number.end());
    copy(number.begin(), number.end(),
         ostream_iterator<int>(cout, "  "));
    cout << endl;

}

int main(int argc, char **argv)
{
    test();
    return 0;
}

