#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>

using std::cout;
using std::endl;
using std::istream_iterator;
using std::ostream_iterator;
using std::vector;
using std::copy;

void test()
{
    vector<int> vec;
    cout << "1111" << endl;
    istream_iterator<int> isi(std::cin);
    cout << "2222" << endl;
    //vector中在尾部插入元素需要使用push_back函数
    /* copy(isi, istream_iterator<int>(), vec.end()); */
    //std::back_inserter底层会调用到push_back函数
    copy(isi, istream_iterator<int>(), std::back_inserter(vec));
    /* cout << "3333" << endl; */
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, "  "));
    /* cout << "4444" << endl; */
    cout << endl;
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

