 ///
 /// @file    MiddleBracket.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 16:08:51
 ///
 
#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class CharArray
{
public:
	CharArray(size_t sz = 10)
	: _data(new char[sz + 1]())
	{	cout << "CharArray(size_t)" << endl;	}

	//std::string和std::vecotr的下标访问运算符都是采用
	//重载的形式完成类似于C数组的用法的
	//
	//下标访问运算符的第二个可以是任意类型
	//
	//后续的map容器可以看到是可以设置为任意类型
	char & operator[](size_t idx)
	{	
		cout << "char & operato[](size_t)" << endl;
		return _data[idx];	
	}

	size_t length() const
	{	return strlen(_data);	}

	~CharArray() 
	{
		cout << "~CharArray()" << endl;
		if(_data) {
			delete [] _data;
			_data = nullptr;
		}
	}

private:
	char * _data;
};
 
void test0() 
{
	//将arr看成是对象
	int arr[5] = {1, 2, 3, 4, 5};
	cout << arr[0] << endl;
	//arr.operator[](0)

	const char * pstr = "hello,world";
	CharArray carr(strlen(pstr));

	for(size_t idx = 0; idx < strlen(pstr); ++idx) 
	{
		carr[idx] = pstr[idx];
	}

	for(size_t idx = 0; idx < carr.length(); ++idx) 
	{
		cout << carr[idx] << " ";
	}
} 
 
int main(void)
{
	test0();
	return 0;
}
