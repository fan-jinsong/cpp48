 ///
 /// @file    FunctionObj.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 11:32:48
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//std::function ==> 函数容器


//面向对象的世界，一切皆对象
//
//对于重载了函数调用运算符的类创建的对象，称为函数对象
//
//函数对象相比于普通函数而言，携带了状态的
//函数对象的类倾向于用struct关键字来定义, 默认访问是public的，
//不用显式给出public关键字
struct Foo
{
	//()被称为函数调用运算符
	int operator()(int x, int y)
	{	
		++_count;
		return x * y;	
	}

	int operator()(int x, int y, int z)
	{
		++_count;
		return x + y + z;
	}

	//c++11之后的用法
	int _count = 0;//状态
	//...
};
 
void test0() 
{
	//普通函数也可以携带状态
	static int number = 0;
	++number;
	cout << "test0()" << endl;
} 

void test0(int x, int y)
{
	cout << "x:" << x << ", y:" << y << endl;
}
 
int main(void)
{
	//将test0看成是一个自定义类类型实例化的对象
	//那对象也可以触发函数调用的行为
	test0();//() 相当于是函数调用的标志
	test0(1, 2);

	Foo foo;
	cout << foo(2, 2) << endl;
	cout << foo(3, 4, 5) << endl;
	
	cout << "foo._count:" << foo._count << endl;

	Foo foo2;//对象的创建可以有无数个,所以状态也是无限的
			 //不同的对象就可以有不同的状态
	return 0;
}
