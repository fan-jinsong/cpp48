 ///
 /// @file    Complex.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 10:04:17
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//数学之中的复数, 负数怎么开方   -1 = i^2
class Complex
{
public:
	Complex(double dreal = 0, double dimage = 0)
	: _dreal(dreal)
	, _dimage(dimage)
	{	cout << "Complex(double,double)" << endl;}

	void print() const
	{
		cout << _dreal;
		if(_dimage > 0) {
			cout << " + " << _dimage << "i" << endl;
		} else if(_dimage == 0){
			return;
		} else {
			cout << " - " <<  _dimage * ( -1 )  << "i" << endl;
		}
	}

	//称为get系列函数
	double real() const {	return _dreal; }
	double image() const {	return _dimage;}
	//定义set系列函数
	void setReal(double r) {	_dreal = r;	}
	void setImage(double i) {	_dimage = i;}

private:
	double _dreal;
	double _dimage;
};


//重载的类型必须是一个自定义类类型或枚举类型
//不能对内置类型的运算符进行重载, 编译报错
//int operator+(int x, int y)
//{	return x - y;}

//普通函数的形式重载
Complex operator+(const Complex & lhs, const Complex & rhs)
{
	cout << "Complex operator+(const Complex&,constComplex&)" << endl;
	return Complex(lhs.real() + rhs.real(),
				   lhs.image() + rhs.image());
}

Complex & operator+=(Complex &lhs, const Complex & rhs)
{
	lhs.setReal(lhs.real() + rhs.real());
	lhs.setImage(lhs.image() + rhs.image());
	return lhs;
}

 
void test0() 
{
	int a = 1, b = 2;
	int c = a + b;

	&(a += b);//a的值发生了变化, a还存在
	cout << (a += b) << endl;//结果是a的值, 即表达式返回的是a

	Complex c1(1, 2), c2(10, -3);
	Complex c3 = c1 + c2;
	cout << "c3:";
	c3.print();

	c1 += c2;
	cout << "\nc1:";
	c1.print();
} 
 
int main(void)
{
	test0();
	return 0;
}
