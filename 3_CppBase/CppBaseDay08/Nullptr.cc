 ///
 /// @file    Nullptr.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 16:01:37
 ///
 
#include <iostream>
using std::cout;
using std::endl;

struct Foo
{
	void test1()
	{	cout << "Foo::test1()" << endl;	}

	void test2(int x)
	{	cout << "Foo::test2(int) x: " << x << endl;	}

	void test3()
	{	cout << "Foo::test3() _data:" << this->_data << endl;}
	
	int _data = 10;
};
 
void test0() 
{
	//当传递空指针时，只要成员函数内部没有涉及到对象的
	//数据成员的访问，都可以正常执行
	Foo * fp = nullptr;
	fp->test1();// Foo::test1(nullptr);
	fp->test2(1);//Foo::test2(nullptr, 1);
	fp->test3();// Foo::test3(nullptr);
} 
 
int main(void)
{
	test0();
	return 0;
}
