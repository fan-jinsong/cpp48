 ///
 /// @file    FunctionObj2.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-27 14:34:47
 ///
 
#include <iostream>
using std::cout;
using std::endl;


struct Foo
{
	void display(int x)
	{	
		cout << "Foo::display()" << endl;
		cout << "x:" << x << endl;	
	}

	void print(int x)
	{	
		cout << "Foo::print()" << endl;
		cout << "x:" << x << endl;	
	}
};

typedef int Integer;

//函数类型：由返回值加上参数列表
typedef void(*Function)();
//需求: 希望用一个指针指向类的成员函数
typedef void(Foo::*Func)(int);

typedef void(*Function)();

//typedef void() * f;

void test1()
{
	Func f = &Foo::print;
	//f->*();
	//对于成员函数指针指向的成员函数，应该要
	//使用一个对象来进行调用
	Foo foo;
	(foo.*f)(1);// .* 成员指针访问运算符

	Foo * fp = &foo;
	f = &Foo::display;
	(fp->*f)(1);// ->*  成员指针访问运算符

}

void display()
{
	cout << "display()" << endl;
}

void print()
{
	cout << "print()" << endl;
}
 
void test0() 
{
	//Function是一个类型，f是一个对象
	Function f(display);
	//Function f = display;
	f();

	f = &print;
	f();
} 
 
int main(void)
{
	/* test0(); */
	test1();
	return 0;
}
