 ///
 /// @file    namespace1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 10:29:43
 ///
 
#include <iostream>
//using编译指令 可以一次性将该空间中的所有实体引入
//
//当我们对某一个空间还不熟悉的时候，就不建议这样使用了
using namespace std;


namespace wd
{
int num = 1;

void display() 
{
	cout << "display()" << endl;
}

}//end of namespace wd

//先定义，再使用
using namespace wd;


//全局作用域
void cout()
{
	printf("new function cout\n");
}
 
void test0() 
{
	cout();
	//cout << num << endl;
	display();
} 
 
int main(void)
{
	test0();
	return 0;
}
