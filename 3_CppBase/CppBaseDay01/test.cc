 ///
 /// @file    test.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 11:23:44
 ///
 
#include <iostream>
using std::cout;
using std::endl;

//extern int gnum;
int gnum = 1;//全局变量

//静态变量只能在本模块内部使用
//静态变量无法跨模块使用的
static int num = 2;//静态变量
 
void test0() 
{
 
} 
 
int main(void)
{
	test0();
	return 0;
}
