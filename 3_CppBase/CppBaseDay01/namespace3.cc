 ///
 /// @file    namespace1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 10:29:43
 ///
 
#include <stdio.h>
#include <iostream>

//第三种方式:using声明机制
//将std空间中的cout实体引入, 只会引入一个
//大力推荐这种方式
using std::cout;
using std::endl;

namespace wd
{
int num = 1;

void display() 
{
	cout << "display()" << endl;
	//C的库函数可以认为就是在匿名命名空间中的
	::printf("printf display()\n");
}

}//end of namespace wd

void test0() 
{
	//只在test0函数中起作用
	using wd::display;
	using wd::num;

	cout << num << endl;
	display();
} 

void test1() 
{
	//display();//error
}
 
int main(void)
{
	test0();
	return 0;
}
