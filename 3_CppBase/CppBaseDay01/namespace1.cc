 ///
 /// @file    namespace1.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-19 10:29:43
 ///
 
#include <iostream>
using namespace std;//using编译指令

namespace wd
{
int num = 1;

void display() 
{
	cout << "display()" << endl;
}

}//end of namespace wd
 
void test0() 
{
	//作用域限定符
	cout << wd::num << endl;
	wd::display();
} 
 
int main(void)
{
	test0();
	return 0;
}
