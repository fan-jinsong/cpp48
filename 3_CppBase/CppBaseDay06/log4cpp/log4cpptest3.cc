 ///
 /// @file    log4cpptest2.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-25 14:51:00
 ///
 
#include <iostream>
using std::cout;
using std::endl;

#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/Category.hh>
#include <log4cpp/Priority.hh>
#include <log4cpp/PatternLayout.hh>

using namespace log4cpp;
 
void test0() 
{	
	PatternLayout * ptn1 = new PatternLayout();
	ptn1->setConversionPattern("%d %c [%p] %m%n");

	PatternLayout * ptn2 = new PatternLayout();
	ptn2->setConversionPattern("%d %c [%p] %m%n");

	PatternLayout * ptn3 = new PatternLayout();
	ptn3->setConversionPattern("%d %c [%p] %m%n");

	//日志目的地
	OstreamAppender * pOsapp = new OstreamAppender("console", &cout);
	//设置日志布局
	pOsapp->setLayout(ptn1);

	//一个目的地要对应一个日志布局(一一对应)
	FileAppender * pFileApp = new FileAppender("pFileApp", "wd.log");
	pFileApp->setLayout(ptn2);

	auto pRollingApp = new RollingFileAppender(
			"pRollingApp",
			"rollingwd.log",
			5 * 1024,
			3);
	pRollingApp->setLayout(ptn3);

	Category & mycat = Category::getRoot().getInstance("SalesDepart");
	//设置Category的优先级
	mycat.setPriority(Priority::DEBUG);
	//添加日志目的地(可以有多个)
	mycat.addAppender(pOsapp);
	mycat.addAppender(pFileApp);
	mycat.addAppender(pRollingApp);

	//通过成员函数来记录日志，成员函数名代表的是本条日志的优先级
	for(int i = 0; i < 100; ++i) {
		mycat.emerg("this is an emerg message");
		mycat.fatal("this is a fatal message");
		mycat.crit("this is a crit message");
		mycat.error("this is an error message");
		mycat.warn("this is a warn message");
		mycat.notice("this is a notice message");
		mycat.info("this is an info message");
		mycat.debug("this is a debug message");
	}

	//可以回收申请的资源
	Category::shutdown();
} 
 
int main(void)
{
	test0();
	return 0;
}
