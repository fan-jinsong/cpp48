 ///
 /// @file    Point.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-04-25 16:30:00
 ///
 
#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

class Point;//类的前向声明
class Line
{
public:
	float getDistance(const Point & lhs, const Point & rhs);
};

class Point
{
public:
	Point(int ix = 0, int iy = 0)
	: _ix(ix)
	, _iy(iy)
	{	cout << "Point(int=0,int=0)" << endl;}

	~Point() {	cout << "~Point()" << endl;	}

	void print() const
	{
		cout << "(" << _ix
			 << "," << _iy
			 << ")";
	}

	//友元之成员函数
	friend float Line::getDistance(const Point & lhs, const Point & rhs);

private:
	int _ix;
	int _iy;
};

float Line::getDistance(const Point & lhs, const Point & rhs)
{
	return sqrt((lhs._ix - rhs._ix) * (lhs._ix - rhs._ix) +
				(lhs._iy - rhs._iy) * (lhs._iy - rhs._iy));
}
 
void test0() 
{
	Point p1(1, 2), p2(3, 4);
	p1.print();
	cout << " --->";
	p2.print();
	/* Line line; */
	//Line();//匿名对象
	cout << "的距离是:" << Line().getDistance(p1, p2) << endl;
} 
 
int main(void)
{
	test0();
	return 0;
}
