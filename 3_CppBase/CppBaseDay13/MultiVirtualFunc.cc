 ///
 /// @file    MultiVirtualFunc.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-05 14:49:14
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class A {
public:
	virtual void a() {	cout << "a() in A" << endl;	}
	virtual void b() {	cout << "b() in A" << endl;	}
	virtual void c() {	cout << "c() in A" << endl;	}
};

class B {
public:
	virtual void a() {	cout << "a() in B" << endl;	}
	virtual void b() {	cout << "b() in B" << endl;	}
	void c() {	cout << "c() in B" << endl;	}
	void d() {	cout << "d() in B" << endl;	}
};

class C
: public A
, public B {
public:
	virtual void a() {	cout << "a() in C" << endl;	}
	void c() override {	cout << "c() in C" << endl;	}
	void d() {	cout << "d() in C" << endl;	}
}; 

class D
: public C
{
public:
	void c() {	cout <<"c() in D" << endl;	}	
};

void test0() 
{
	C c;
	c.a();
	//c.b();//error二义性, 通过对象调用时，与虚函数无关
	c.c();
	c.d();
	cout << endl;

	A * pa = &c;
	pa->a();
	pa->b();
	pa->c();//通过A指针调用c时，解释成虚函数
	//pa->d();
	cout << endl;
	
	B * pb = &c;
	pb->a();
	pb->b();
	pb->c();//B::c()  通过B指针调用c时，解释成非虚函数
	pb->d();
	cout << endl;
	C * pc = &c;
	pc->a();
	//pc->b();//error二义性
	pc->c();//解释成虚函数
	pc->d();//非虚函数

	cout << endl;
	D d;
	pc = &d;
	pc->c();
} 
 
int main(void)
{
	test0();
	return 0;
}


