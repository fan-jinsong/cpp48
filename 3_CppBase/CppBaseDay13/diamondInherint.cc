 ///
 /// @file    diamondInherint.cc
 /// @author  lemon(haohb13@gmail.com)
 /// @date    2023-05-05 17:08:19
 ///
 
#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
	explicit
	A(long ia)
	: _ia(ia)
	{	cout << "A(long)" << endl;	}

	~A() {	cout << "~A()" << endl;	}

private:
	long _ia;
};

class B
: virtual public A
{
public:
	B(long ia, long ib)
	: A(ia)
	, _ib(ib)
	{	cout << "B(long,long)" << endl;}

	~B() {cout << "~B()" << endl;	}

private:
	long _ib;
};

class C
: virtual public A
{
public:
	C(long ia, long ic)
	: A(ia)
	, _ic(ic)
	{	cout << "C(long,long)" << endl;}

	~C() {cout << "~C()" << endl;	}

private:
	long _ic;
};

class D
: public B
, public C
{
public:
	D(long ix1, long ix2, long iy1, long iy2, long id)
	: B(ix1, ix2) //B中会初始化A
	, C(iy1, iy2) //C中也会初始化A
	, A(ix1) //采用虚拟继承时，A的初始化必须要放在D的构造函数中进行
	, _id(id)//如果不写，就会自动调用A的默认构造函数
	{	cout << "D(long,long,long, long, long)" << endl;}

	~D()
	{	cout << "~D()" << endl;	}

private:
	long _id;
};
 
void test0() 
{
	D d(1, 2, 3, 4, 5);
} 
 
int main(void)
{
	test0();
	return 0;
}
